//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "LeafShaderScene.h"

#include "GameComponents\World.h"
#include "Components\Components.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameObject.h"
#include "../Materials/LeafMaterial.h"
#include "../Materials/DiffuseMaterial.h"
#include "Content/ContentManager.h"
#include "GameComponents\HeroCharacter.h"

LeafShaderScene::LeafShaderScene(void):
	GameScene(L"LeafShaderScene"),
	m_LeafMaterialSize(6),
	m_TreeMeshSize(5),
	m_AmmountOfTrees(20),
	m_TreeOffset(300)
{
}

LeafShaderScene::~LeafShaderScene(void)
{
	m_Materials.clear();
}

void LeafShaderScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	

	for (UINT i = 0; i < m_LeafMaterialSize; ++i)
	{
		AddLeafMaterial(gameContext);
	}

	for (UINT i = 0; i < m_TreeMeshSize; ++i)
	{
		AddTreeMesh();
	}


	for (UINT i = 0; i < m_AmmountOfTrees; i++)
	{
		GameObject *treeObject = new GameObject();
		ModelComponent *treeComponent = new ModelComponent(m_vTreeModels[rand()%m_vTreeModels.size()]);
		treeComponent->SetMaterial(rand()%m_LeafMaterialSize+10);
		treeObject->AddComponent(treeComponent);
		treeObject->GetTransform()->Translate(m_TreeOffset*(i % 4) + ((rand() % 200)-100), 0, m_TreeOffset * (i / 4) + ((rand() % 200)-100));

		AddChild(treeObject);
	}

	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto bouncyMaterial = physX->createMaterial(0, 0, 1);
	auto ground = new GameObject();
	ground->AddComponent(new RigidBodyComponent(true));

	std::shared_ptr<PxGeometry> geom(new PxPlaneGeometry());
	ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));
	ground->GetTransform()->Translate(0, 40, 0);
	AddChild(ground);
}

void LeafShaderScene::AddLeafMaterial(const GameContext& gameContext){

	//lEVEL CREATION
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto defaultMaterial = physX->createMaterial(0.5, 0.5, 1);


	auto LevelObject = new GameObject();
	auto LevelRigidbody = new RigidBodyComponent(true);
	LevelObject->AddComponent(LevelRigidbody);
	std::shared_ptr<PxGeometry> geom(new PxPlaneGeometry());
	LevelObject->AddComponent(new ColliderComponent(geom, *defaultMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));

	AddChild(LevelObject);

	LeafMaterial *leafMaterial = new LeafMaterial();

	leafMaterial->SetDiffuseTextureTree(L"./TheGame/Textures/TreeTextures/TreeTexture.dds");
	leafMaterial->SetDiffuseTextureLeaf(L"./TheGame/Textures/TreeTextures/LeafDifuse" + to_wstring(m_LeafCounter) + L".dds");
	leafMaterial->SetLeafOpacity(L"./TheGame/Textures/TreeTextures/LeafOpacity" + to_wstring(m_LeafCounter) + L".dds");

	leafMaterial->SetLeafSize(rand()%10);
	leafMaterial->SetTreeSize(XMFLOAT2(100.f, 200.f));
	gameContext.pMaterialManager->AddMaterial(leafMaterial, 10+m_LeafCounter);
	m_Materials.push_back(leafMaterial);
	m_LeafCounter++;


}	

void LeafShaderScene::AddTreeMesh(){
	m_vTreeModels.push_back(ContentManager::Load<MeshFilter>(L"./TheGame/Textures/TreeTextures/Tree" + to_wstring(m_vTreeModels.size()) + L".ovm"));
}

void LeafShaderScene::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//AddChild(m_pBuildingFactory->CreateHumanBarracks());

	float treeGrowth = 5.f;

	for (auto material : m_Materials)
	{
		auto leafSize = material->GetLeafSize();
		auto treeSize = material->GetTreeSize();

		if (leafSize > 15)
			leafSize = 0;
		if (treeSize.x > 2000)
			treeSize.x = 200;
		leafSize += gameContext.pGameTime->GetElapsed();
		treeSize = XMFLOAT2(treeSize.x, treeSize.y + gameContext.pGameTime->GetElapsed()*50);

		material->SetLeafSize(leafSize);
		//material->SetTreeSize(m_currentTreesize);
	}
}

void LeafShaderScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
