//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
#pragma once
#include "Scenegraph/GameScene.h"

class MeshFilter;
class LeafMaterial;

class LeafShaderScene : public GameScene
{
public:
	LeafShaderScene(void);
	virtual ~LeafShaderScene(void);

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

	
private:

	void AddLeafMaterial(const GameContext& gameContext);
	void AddTreeMesh();


	UINT m_LeafCounter = 0;
	UINT m_LeafMaterialSize;
	UINT m_TreeMeshSize;
	UINT m_AmmountOfTrees;

	float m_TreeOffset;
	vector<MeshFilter*> m_vTreeModels;
	vector<LeafMaterial*> m_Materials;

	float m_currentLeafSize = 0;
	XMFLOAT2 m_currentTreesize = XMFLOAT2(0,0);
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	LeafShaderScene( const LeafShaderScene &obj);
	LeafShaderScene& operator=( const LeafShaderScene& obj);
};


