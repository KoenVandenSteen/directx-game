//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "TheGameScene.h"

#include "GameComponents\World.h"
#include "Components\Components.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameObject.h"
#include "GameComponents\ClothObject.h"
#include "Components/RigidBodyComponent.h"
#include "Components/ColliderComponent.h"
#include "Buildings\BuildingFactory.h"
#include "Buildings\Building.h"
#include "GameComponents\HeroCharacter.h"	
#include "GameComponents\MovingWall.h"
#include "GameComponents\GoldPickUp.h"
#include "../../OverlordEngine/Base/OverlordGame.h"
#include "GameComponents\Interface.h"
#include "GameComponents\ParticleManager.h"
#include "GameComponents\ParticleEffect.h"
#include "Units\UnitManager.h"

#include "Buildings\HumanBarracks.h"
#include "Buildings\HumanRange.h"
#include "Buildings\HumanSiegeShop.h"
#include "Buildings\HumanStables.h"

#include "GameComponents\EnemyPlayer.h"

#include "../Materials/BlurMaterial.h"
#include "../Materials/SepiaMaterial.h"
#include "../Materials/GreyMaterial.h"
#include "../../OverlordEngine/Graphics/PostProcessingMaterial.h"

#include "GameComponents\Arrow.h"

TheGameScene::TheGameScene(void) :
GameScene(L"TheGameScene"),
m_ammountOfCoins(25),
m_coinStartValue(4.0f),
m_respawnWall(3.f),
m_wallRespawnTimer(0.f),
m_StartingHealth(200.f),
m_PlayerBaseHealth(200.f),
m_EnemyBaseHealth(200.f),
m_StartingGold(50)
{

}

TheGameScene::~TheGameScene(void)
{

}

void TheGameScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	GetPhysxProxy()->EnablePhysxDebugRendering(true);
	GetPhysxProxy()->GetPhysxScene()->setVisualizationParameter(PxVisualizationParameter::eCLOTH_BENDING, true);
	GetPhysxProxy()->GetPhysxScene()->setVisualizationParameter(PxVisualizationParameter::eCLOTH_HORIZONTAL, true);
	GetPhysxProxy()->GetPhysxScene()->setVisualizationParameter(PxVisualizationParameter::eCLOTH_VERTICAL, true);

	//GetPhysxProxy()->GetPhysxScene()->setFlag(PxSceneFlag::eENABLE_KINEMATIC_PAIRS,true);
	//GetPhysxProxy()->GetPhysxScene()->setFlag(PxSceneFlag::eENABLE_KINEMATIC_STATIC_PAIRS, true);
	//auto physX = PhysxManager::GetInstance()->GetPhysics();

	//auto defaultMaterial = physX->createMaterial(0.5, 0.5, 1);

	auto clothObject1 = new ClothObject(XMFLOAT4(0,0,1,1),PxVec3(120,0,-250));
	AddChild(clothObject1);

	auto clothObject2 = new ClothObject(XMFLOAT4(0, 0, 1, 1), PxVec3(-120, 0, -250));
	AddChild(clothObject2);
	
	auto clothObject3 = new ClothObject(XMFLOAT4(1, 0, 0, 1), PxVec3(120, 0, 850));
	AddChild(clothObject3);

	auto clothObject4 = new ClothObject(XMFLOAT4(1, 0, 0, 1), PxVec3(-120, 0, 850));
	AddChild(clothObject4);

	m_pWorld = new World();
	AddChild(m_pWorld);

	//adding the hero to the scene
	m_pHero = new HeroCharacter();
	m_pHero->GetTransform()->Translate(0, 10, -400);
	AddChild(m_pHero);

	m_currentHeroState = HeroState::BuildStage;

	auto goldmat = new DiffuseMaterial();

	gameContext.pMaterialManager->AddMaterial(goldmat, 58);
	goldmat->SetDiffuseTexture(L"./TheGame/Textures/GoldTexture.dds");


	auto walmat = new DiffuseMaterial();

	gameContext.pMaterialManager->AddMaterial(walmat, 59);
	walmat->SetDiffuseTexture(L"./TheGame/Textures/CaveTexture.dds");
	
	m_pInterface = GetChild<Interface>();

	AddParticleEffects();


	auto UnitManager = UnitManager::GetInstance();
	AddChild(UnitManager);

	m_currentGameState = GameState::Start;
	m_pHero->SetGold(m_StartingGold);

	auto enemyPlayer = new EnemyPlayer();
	AddChild(enemyPlayer);

	AddChild(BuildingFactory::GetInstance());


	auto inputAction = InputAction(PostProcessingDraw::Grey, InputTriggerState::Pressed, VK_F10);
	gameContext.pInput->AddInputAction(inputAction);

	inputAction = InputAction(PostProcessingDraw::Blur, InputTriggerState::Pressed, VK_F11);
	gameContext.pInput->AddInputAction(inputAction);

	inputAction = InputAction(PostProcessingDraw::Sepia, InputTriggerState::Pressed, VK_F12);
	gameContext.pInput->AddInputAction(inputAction);

	m_pBlurMat = new BlurMaterial();
	m_pGreyMat = new GreyMaterial();
	m_pSepiaMat = new SepiaMaterial();

	AddPostProcessingEffect(m_pBlurMat);
	AddPostProcessingEffect(m_pGreyMat);
	AddPostProcessingEffect(m_pSepiaMat);


}

void TheGameScene::AddParticleEffects()
{
	auto particleManager = ParticleManager::GetParticleManager();
	AddChild(particleManager);
	particleManager->AddParticleEffect("bloodSplat", XMFLOAT3(1.0f, -2.0f, 1.0f), 1.f, 2.f, 1.f, 2.f, 3.5f, 5.5f, 0.2f, 0.5f, XMFLOAT4(1.f, 0.f, 0.f, 0.6f), L"./TheGame/Textures/BloodParticle.dds");
}


void TheGameScene::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//AddChild(m_pBuildingFactory->CreateHumanBarracks());

	if (m_pWorld->m_EntranceTriggered){
		m_pHero->GetTransform()->Translate(500, 10, -100);
		m_pWorld->m_EntranceTriggered = false;
		m_currentHeroState = HeroState::CoinStage;
		m_EnteredGoldStage = true;
	}
	
	switch (m_currentHeroState)
	{
	case TheGameScene::BuildStage:
		if (m_vGoldPickups.size() != 0)
		{
			for (UINT i = 0; i < m_vGoldPickups.size(); ++i)
			{
				if (m_vGoldPickups.at(i) != nullptr){
					RemoveChild(m_vGoldPickups.at(i));
					m_vGoldPickups.erase(m_vGoldPickups.begin() + i);
				}
			}
		
		}
		break;
	case TheGameScene::CoinStage:
		if (m_EnteredGoldStage)
			SpawnGoldStageEvent();
			m_EnteredGoldStage = false;
			MovingWallEvent(gameContext.pGameTime->GetElapsed());
			CheckCoins();
		break;
	default:
		break;
	}
	
	if (m_ZoomIn)
		m_pHero->zoomCameraIn(gameContext);
	else
		m_pHero->zoomCameraOut(gameContext);
	
	CheckCameraRay(gameContext);
	
	m_pInterface->SetGold((int)m_pHero->GetGold());
	

	XMFLOAT2 look = XMFLOAT2(0, 0);
	if (gameContext.pInput->IsMouseButtonDown(VK_LBUTTON) && m_currentGameState != Play)
	{
		auto mouseMove = gameContext.pInput->GetMousePosition();
		look.x = static_cast<float>(mouseMove.x);
		look.y = static_cast<float>(mouseMove.y);
		//cout << look.x << " : " << look.y << endl;

		if (look.x > 429.f && look.x < 817.f && look.y > 437 && look.y < 582)
		{
			ResetGame();
			SetCurrentGameState(Play);
		}
	}

	m_pInterface->SetBaseHitPoints(static_cast<int>(m_PlayerBaseHealth));
	m_pInterface->SetEnemenyBaseHitPoints(static_cast<int>(m_EnemyBaseHealth));

	if (m_PlayerBaseHealth < 0 || m_EnemyBaseHealth < 0 )
	{
		m_currentGameState = End;
	}


	
	if (gameContext.pInput->IsActionTriggered(PostProcessingDraw::Blur))
	{
		if (m_pBlurMat->DrawEnabled())
			m_pBlurMat->EnableDraw(false);
		else
			m_pBlurMat->EnableDraw(true);
	}


	if (gameContext.pInput->IsActionTriggered(PostProcessingDraw::Grey))
	{
		if (m_pGreyMat->DrawEnabled())
			m_pGreyMat->EnableDraw(false);
		else
			m_pGreyMat->EnableDraw(true);
	}


	if (gameContext.pInput->IsActionTriggered(PostProcessingDraw::Sepia))
	{
		if (m_pSepiaMat->DrawEnabled())
			m_pSepiaMat->EnableDraw(false);
		else
			m_pSepiaMat->EnableDraw(true);
	}


}


void TheGameScene::CheckCameraRay(const GameContext& gameContext)
{
	auto physxProxy = GetPhysxProxy();
	auto camPos = gameContext.pCamera->GetTransform()->GetWorldPosition();
	auto camForward = gameContext.pCamera->GetTransform()->GetForward();
	auto heroPos = m_pHero->GetTransform()->GetPosition();

	PxVec3 rayStart = PxVec3(camPos.x, camPos.y, camPos.z);
	PxVec3 rayEnd = PxVec3(heroPos.x, heroPos.y, heroPos.z);

	PxVec3 rayDir = (rayEnd-rayStart);
	rayDir.normalize();

	PxRaycastBuffer hit;

	if (physxProxy->Raycast(rayStart, rayDir, PX_MAX_F32, hit)){
		auto obj =  ((BaseComponent*)hit.block.actor->userData)->GetGameObject();
		if (obj == m_pHero){
			m_ZoomIn = false;
		}
		else{
			m_ZoomIn = true;
		}
	}
}
void TheGameScene::CheckCoins(){

	for (UINT i = 0; i < m_vGoldPickups.size(); ++i)
	{
		if (m_vGoldPickups.at(i) != nullptr && m_vGoldPickups.at(i)->IsPickedUp()){
			RemoveChild(m_vGoldPickups.at(i));
			m_vGoldPickups.erase(m_vGoldPickups.begin()+i);
			}
	}
		
}
void TheGameScene::MovingWallEvent(float time){
	
	m_wallRespawnTimer += time;
	if (m_wallRespawnTimer > m_respawnWall){
		int randXOffset = rand() % 50;
		auto movingWall = new MovingWall(59);
		movingWall->GetTransform()->Translate(475.f + randXOffset, 0, 200.f);
		AddChild(movingWall);
		m_wallRespawnTimer = 0;
	}
	
}

void TheGameScene::SetCurrentHeroState(HeroState stateToSet){
	if (m_currentHeroState == HeroState::CoinStage)
		ResetHero();
	
	m_currentHeroState = stateToSet;
}

void TheGameScene::ResetHero(){
	m_CoinMultiplier = 1;
	m_pHero->GetTransform()->Translate(-200, 30, -400);
}


void TheGameScene::SpawnGoldStageEvent(){

	m_vGoldPickups.clear();
	float coinOffset = 15.0f;

	for (UINT i = 0; i < m_ammountOfCoins; i++)
	{
		auto testCoin = new GoldPickUp(m_coinStartValue * i,58);
		int xOffset =  rand() % 90;
		testCoin->GetTransform()->Translate(455+(float)xOffset, 5, -180 + i * coinOffset);
		AddChild(testCoin);
		m_vGoldPickups.push_back(testCoin);
	}

}


void TheGameScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}


void TheGameScene::GoldPickedUp(float ammount){

	m_pHero->SetGold(m_pHero->GetGold() + ammount * m_CoinMultiplier);
	m_CoinMultiplier+= 0.1f;
}


void TheGameScene::SetCurrentGameState(GameState gameState){
	
	if (m_currentGameState != Play ){
		ResetGame();
	}

	m_currentGameState = gameState;
}

void TheGameScene::ResetGame(){

	ResetHero();

	m_currentHeroState = BuildStage;
	
	UnitManager::GetInstance()->Reset();
	
	m_PlayerBaseHealth = m_StartingHealth;
	m_EnemyBaseHealth = m_StartingHealth;
	m_pHero->SetGold(m_StartingGold);
	m_pHero->Reset();
	GetChild<EnemyPlayer>()->Reset();
	GetChild<World>()->Reset();

}