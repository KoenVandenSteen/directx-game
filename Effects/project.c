#include "reg832.h"
#include "project.h"
#include <string.h>
#include "font.h"


int main(void)
{
	boot();

	while(1);
}

void lcd_enable(void)
{    
    wait (1);
    ENABLE=1;
	wait (1);
    ENABLE=0;
	wait (1);
}

unsigned char lcd_status(void)
{
    unsigned char status;
    LCD_DATA=0xff;
    DI=0; 
	RW=1;
    ENABLE = 1;
    status = LCD_DATA;
	ENABLE = 0;
    return status;
}

void lcd_reset(void)
{
	wait (100);
    RESET=1;
	wait (100);
    RESET=0;
    while (lcd_status() & 0x10)
    {
		wait (100);
    }
}

void lcd_screenon(unsigned char on)
{
    RW=0; DI=0;
    LCD_DATA  = 0x3e | (on & 0x01);
    lcd_enable();
}

void lcd_cls(void)
{
    unsigned char x,y;
	lcd_selectside(BOTH);
    for (x=0; x<8; x++)
    {
        lcd_setpage(x);
        lcd_setyaddr(0);
        LCD_DATA=0; 
		RW=0; 
		DI=1;
        for (y=0; y<64; y++)
        {
            lcd_enable(); 
            wait(1);
        }
    }
}

void lcd_setpage(unsigned char x)
{
    lcd_waitbusy();
    DI=0; RW=0; 
    LCD_DATA = 0xb8 | x;
    lcd_enable();
}

void lcd_setyaddr(unsigned char y)
{
    lcd_waitbusy();
    DI=0; RW=0;
    LCD_DATA = 0x40 | (y & 0x3f);
    lcd_enable();
}

void lcd_waitbusy(void)
{
    while (lcd_status() & 0x80)
    {   
	wait (1);
    }
}

void lcd_write (unsigned char info)
{
    lcd_waitbusy();
    DI=1; RW=0;
    LCD_DATA = info;
    lcd_enable();
}

void lcd_selectside(unsigned char sides)
{
    if (sides & LEFT)
        CS1 = 1;
    else
        CS1 = 0;
		
    if (sides & RIGHT)
        CS2 = 1;
    else
        CS2 = 0;
}

unsigned char lcd_read (void)
{
    unsigned char _data;
	LCD_DATA=0xff;
    RW = 1; 
	DI=1;
    //lcd_enable();
	ENABLE = 1;
    _data = LCD_DATA;
	ENABLE = 0;
    return _data;
}

void lcd_putchar(char c)
{
    int base;
    base = c - 32;
    base *= 3;

    lcd_write(font[base]);
    lcd_write(font[base + 1]);
    lcd_write(font[base + 2]);
    lcd_write(0);
}

void lcd_plotpixel(unsigned char rx, unsigned char ry)
{
    unsigned char info;

    //lcd_waitbusy();

    // select the correct side
    if (rx & 64)
        lcd_selectside(RIGHT);
    else
        lcd_selectside(LEFT);

 
    lcd_setpage( ry >> 3);
    lcd_setyaddr( rx & 0x3f);
    info = lcd_read(); // dummy read needed here
    info = lcd_read();

    lcd_setyaddr( rx & 0x3f); 
    lcd_write (info | (1 << (ry & 0x7)));
}

void lcd_plottext(unsigned char rx, unsigned char ry, char array[])
{
	char i=0,t=rx,x;
	int base;
    unsigned char info;

	if (t >= 64)
	{
		lcd_selectside(RIGHT);
		lcd_setyaddr( (rx-64) & 0x3f);
	}
	else
	{
		lcd_selectside(LEFT);
		lcd_setyaddr( rx & 0x3f);
	}
	for(i=0;i<strlen(array);i++)
	{
		base = (array[i]-32)*3;
		for(x=0;x<4;x++)
		{
			if (t == 64)
			{
				lcd_selectside(RIGHT);
				lcd_setyaddr(0);
			}
			if (t >= 64)
				lcd_selectside(RIGHT);
			else
				lcd_selectside(LEFT);
			//lcd_setpage(ry/8);
			//lcd_setyaddr(t);
			//info = lcd_read();
			//info = lcd_read();
			lcd_setpage(ry/8);
			lcd_setyaddr(t);
			if(x<3){lcd_write(info|(font[base+x]<<(ry%8)));}else{lcd_write(0);}
			t++;
		}
	}
	if(ry%8!=0)
	{
	t=rx;
	if (t >= 64)
	{
		lcd_selectside(RIGHT);
		lcd_setyaddr( (rx-64) & 0x3f);
	}
	else
	{
		lcd_selectside(LEFT);
		lcd_setyaddr( rx & 0x3f);
	}
	
	for(i=0;i<strlen(array);i++)
	{
		base = (array[i]-32)*3;
		for(x=0;x<4;x++)
		{
			if (t == 64)
			{
				lcd_selectside(RIGHT);
				lcd_setyaddr(0);
			}
			if (t >= 64)
				lcd_selectside(RIGHT);
			else
				lcd_selectside(LEFT);
			//lcd_setpage(ry/8+1);
			//lcd_setyaddr(t);
			//info = lcd_read();
			//info = lcd_read();
			lcd_setpage(ry/8+1);
			lcd_setyaddr(t);
			if(x<3){lcd_write(info|(font[base+x]>>(8-(ry%8))));}else{lcd_write(0);}
			t++;
		}
	}
	}
}

void ingamemenu()
{
	char button=1,c=0;
	lcd_cls();
	lcd_plottext(44,15,"Resume Game");
	lcd_plottext(44,30,"Highscores");
	lcd_plottext(40,45,"Instructions");
	while(1)
	{
		if(button==1&&c==0)
		{
			lcd_plottext(33,15,">");
			lcd_plottext(91,15,"<");
			c=1;
		}
		if(button==2&&c==0)
		{
			lcd_plottext(33,30,">");
			lcd_plottext(91,30,"<");
			c=1;
		}
		if(button==3&&c==0)
		{
			lcd_plottext(33,45,">");
			lcd_plottext(91,45,"<");
			c=1;
		}
		if(DOWNB==1)
		{
			button++;
			if(button==4){button=1;}
			c=0;
			lcd_plottext(33,15," ");
			lcd_plottext(91,15," ");
			lcd_plottext(33,30," ");
			lcd_plottext(91,30," ");
			lcd_plottext(33,45," ");
			lcd_plottext(91,45," ");
		}
		if(UPB==1)
		{
			button--;
			if(button==0){button=3;}
			c=0;
			lcd_plottext(33,15," ");
			lcd_plottext(91,15," ");
			lcd_plottext(33,30," ");
			lcd_plottext(91,30," ");
			lcd_plottext(33,45," ");
			lcd_plottext(91,45," ");
		}
		if(AB==1)
		{
			if(button==1){return;}
			if(button==2){highscores();}
			if(button==3){instructions();}
			lcd_cls();
			button=1;
			lcd_plottext(44,15,"Start Game");
			lcd_plottext(44,30,"Highscores");
			lcd_plottext(40,45,"Instructions");
		}
	}
}

void gameover(char score)
{
	 lcd_plottext(46,29,"GAME OVER");
	 lcd_plottext(42,39,"you failed!");
}

void movesnake(char direction, char grow)
{

}

char checkdestination(char direction)
{
	
}

void game()
{
	char t,score=0;
	lcd_cls();
	for(t=0;t<64;t++){lcd_plotpixel(96,t);}
	for(t=0;t<16;t++){lcd_plotpixel(4+t,56);}
	for(t=0;t<16;t++){lcd_plotpixel(4+t,57);}
	for(t=0;t<16;t++){lcd_plotpixel(4+t,58);}
	for(t=0;t<16;t++){lcd_plotpixel(4+t,59);}
	lcd_plottext(100,4,"score:");
	lcd_plottext(100,12,"0");
	lcd_plottext(100,20,"Best:");
	lcd_plottext(100,28,"1:");
	lcd_plottext(100,36,"2:");
	lcd_plottext(100,44,"3:");
	snakex=56;
	snakey=20;
	tailx=56;
	tailx=4;
	direction = 2;
	tail_direction = 2;
	lcd_plottext(4,4,"Press A to start");
	while(1)
	{
		if(AB==1){break;}
	}
	lcd_plottext(4,4,"                ");
	while(1)
	{
		if(UPB==1){direction=1;}
		if(RIGHTB==1){direction=2;}
		if(DOWNB==1){direction=3;}
		if(LEFTB==1){direction=4;}
		if(checkdestination(direction)==0){gameover(score);}
		if(checkdestination(direction)==1){movesnake(direction,0);}
		if(checkdestination(direction)==2){movesnake(direction,1);score++;}
		if(BB==1){ingamemenu();}
	}
}

void highscores()
{
	lcd_cls();
	while(1)
	{
		if(BB==1){return;}
	}
}

void instructions()
{
	lcd_cls();
	lcd_plottext(4,4,"Instructions (B=back):");
	lcd_plottext(4,12,"- Use the arrow keys to control");
	lcd_plottext(4,20,"  the snake");
	lcd_plottext(4,28,"- Use A to boost the snake");
	lcd_plottext(4,36,"- Use B to Pause the game and");
	lcd_plottext(4,44,"  enter the menu");
	while(1)
	{
		if(BB==1){return;}
	}
}

void menu()
{
	char button=1,c=0;
	lcd_plottext(44,15,"Start Game");
	lcd_plottext(44,30,"Highscores");
	lcd_plottext(40,45,"Instructions");
	UPB=0;DOWNB=0;LEFTB=0;RIGHTB=0;AB=0;BB=0;
	while(1)
	{
		if(button==1&&c==0)
		{
			lcd_plottext(33,15,">");
			lcd_plottext(91,15,"<");
			c=1;
		}
		if(button==2&&c==0)
		{
			lcd_plottext(33,30,">");
			lcd_plottext(91,30,"<");
			c=1;
		}
		if(button==3&&c==0)
		{
			lcd_plottext(33,45,">");
			lcd_plottext(91,45,"<");
			c=1;
		}

		if(UPB==1)
		{
			button++;
			if(button==4){button=1;}
			c=0;
			lcd_plottext(33,15," ");
			lcd_plottext(91,15," ");
			lcd_plottext(33,30," ");
			lcd_plottext(91,30," ");
			lcd_plottext(33,45," ");
			lcd_plottext(91,45," ");
		}
		if(DOWNB==1)
		{
			button--;
			if(button==0){button=3;}
			c=0;
			lcd_plottext(33,15," ");
			lcd_plottext(91,15," ");
			lcd_plottext(33,30," ");
			lcd_plottext(91,30," ");
			lcd_plottext(33,45," ");
			lcd_plottext(91,45," ");
		}
		if(AB==1)
		{
			if(button==1){game();}
			if(button==2){highscores();}
			if(button==3){instructions();}
			lcd_cls();
			button=1;
			lcd_plottext(44,15,"Start Game");
			lcd_plottext(44,30,"Highscores");
			lcd_plottext(40,45,"Instructions");
		}
	}
}

void boot()
{
	char a=0,b=36;
	lcd_waitbusy();
    DI=0; RW=0; 
    LCD_DATA = 0xC0;
    lcd_enable();

	lcd_screenon(1);
	lcd_cls();
	
	lcd_plottext(48,29,"PLAY-BOY");

	while(b>0)
	{
		lcd_plotpixel(a+45,26);
		a++;
		lcd_plotpixel(b+45,36);
		b--;
		wait(200);
	}
	a=0;
	b=10;
	while(b>0)
	{
		lcd_plotpixel(81,a+26);
		a++;
		lcd_plotpixel(45,b+26);
		b--;
		wait(200);
	}
	lcd_plottext(32,40,"Jelle Beukeleirs");
	lcd_plottext(28,48,"Koen Van Den Steen");
	wait(10000);
	wait(10000);
	for(a=0;a<17;a++)
	{
		lcd_selectside(LEFT);
		lcd_setyaddr(45+a+1);
		lcd_setpage(3);
		lcd_write(252);
		lcd_setyaddr(45+a+1);
		lcd_setpage(4);
		lcd_write(31);
		lcd_selectside(LEFT);
		lcd_setyaddr(45+a);
		lcd_setpage(3);
		lcd_write(0);
		lcd_setyaddr(45+a);
		lcd_setpage(4);
		lcd_write(0);

		lcd_selectside(RIGHT);
		lcd_setyaddr(81-a-1);
		lcd_setpage(3);
		lcd_write(252);
		lcd_setyaddr(81-a-1);
		lcd_setpage(4);
		lcd_write(31);
		lcd_selectside(RIGHT);
		lcd_setyaddr(81-a);
		lcd_setpage(3);
		lcd_write(0);
		lcd_setyaddr(81-a);
		lcd_setpage(4);
		lcd_write(0);
		
		wait(250);
	}
	lcd_selectside(LEFT);
	lcd_setyaddr(45+a+1);
	lcd_setpage(3);
	lcd_write(252);
	lcd_setyaddr(45+a+1);
	lcd_setpage(4);
	lcd_write(31);
	lcd_selectside(LEFT);
	lcd_setyaddr(45+a);
	lcd_setpage(3);
	lcd_write(0);
	lcd_setyaddr(45+a);
	lcd_setpage(4);
	lcd_write(0);
	
	wait(250);

	lcd_selectside(LEFT);
	lcd_setyaddr(45+a+1);
	lcd_setpage(3);
	lcd_write(0);
	lcd_setyaddr(45+a+1);
	lcd_setpage(4);
	lcd_write(0);
	lcd_selectside(RIGHT);
	lcd_setyaddr(81-a);
	lcd_setpage(3);
	lcd_write(0);
	lcd_setyaddr(81-a);
	lcd_setpage(4);
	lcd_write(0);
	
	lcd_cls();
	menu();
}







