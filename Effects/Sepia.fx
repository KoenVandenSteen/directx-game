Texture2D gTexture;

SamplerState samLinear
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
};

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
/// Create Rasterizer State (Backface culling) 
RasterizerState backCull
{
	CullMode = BACK;
};

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

struct VS_INPUT_STRUCT
{
    float3 Pos : POSITION;
	float2 Tex : TEXCOORD0;

};
struct PS_INPUT_STRUCT
{
    float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD1;
};

PS_INPUT_STRUCT VS(VS_INPUT_STRUCT input)
{
	PS_INPUT_STRUCT output = (PS_INPUT_STRUCT)0;
	// Set the Position
	output.Pos = float4(input.Pos.x, input.Pos.y, input.Pos.z, 1.0f);

	// Set the Text coord
	output.Tex = input.Tex;
	return output;
}

float4 PS(PS_INPUT_STRUCT input):SV_TARGET
{
	float3 color = float3(0,0,0);
	float colorOpacity = 1;

	color = gTexture.Sample(samLinear, input.Tex);

	float3 outputColor = color;
	outputColor.r = (color.r * 0.393) + (color.g * 0.769) + (color.b * 0.189);
	outputColor.g = (color.r * 0.349) + (color.g * 0.686) + (color.b * 0.168);
	outputColor.b = (color.r * 0.272) + (color.g * 0.534) + (color.b * 0.131);


	return float4(outputColor, colorOpacity);
}

technique11 Sepia
{
    pass P0
    {
		// Set states
		SetRasterizerState(backCull);
		SetDepthStencilState(EnableDepth, 0);
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}