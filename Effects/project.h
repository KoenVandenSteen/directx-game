#define LCD_DATA    P2
#define ENABLE      P3_5
#define RW          P3_6
#define DI          P3_7
#define RESET       P3_2
#define CS1      	P3_4
#define CS2     	P3_3
 
#define LEFT        0x1  
#define RIGHT       0x2
#define BOTH        0x3
#define NONE        0x0

#define UPB			P0_5
#define DOWNB		P0_4
#define LEFTB		P0_2
#define RIGHTB		P0_7
#define AB			P0_6
#define BB			P0_3

char direction,tail_direction,snakex,snakey,tailx,taily,bendx[30],bendy[30],bend_direction[30],lastdir;

void lcd_enable(void);
unsigned char lcd_status(void);
void lcd_reset(void);
void lcd_screenon(unsigned char on);
void lcd_cls(void);
void lcd_setpage(unsigned char x);
void lcd_setyaddr(unsigned char y);
void lcd_waitbusy(void);
void lcd_write (unsigned char info);
void lcd_selectside(unsigned char sides);
unsigned char lcd_read (void);
void lcd_putchar(char c);
void lcd_plotpixel(unsigned char rx, unsigned char ry);
void lcd_plottext(unsigned char rx, unsigned char ry, char array[]);
void menu();
void boot();
void game();
void highscores();
void instructions();
void ingamemenu();
void movesnake(char direction, char grow);
char checkdestination(char direction);
void gameover(char score);


void wait (int time){
	int i;
	for (i=0;i<time;i++);	// time keer niets doen.
}
