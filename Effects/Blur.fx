Texture2D gTexture;

SamplerState samLinear
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
};

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
/// Create Rasterizer State (Backface culling) 
RasterizerState backCull
{
	CullMode = BACK;
};

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

struct VS_INPUT_STRUCT
{
    float3 Pos : POSITION;
	float2 Tex : TEXCOORD0;

};
struct PS_INPUT_STRUCT
{
    float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD1;
};

PS_INPUT_STRUCT VS(VS_INPUT_STRUCT input)
{
	PS_INPUT_STRUCT output = (PS_INPUT_STRUCT)0;
	// Set the Position
	output.Pos = float4(input.Pos.x, input.Pos.y, input.Pos.z, 1.0f);

	// Set the Text coord
	output.Tex = input.Tex;
	return output;
}

float4 PS(PS_INPUT_STRUCT input):SV_TARGET
{
	float3 color = float3(0,0,0);
	float colorOpacity = 1;
	
	// Step 1: find the dimensions of the texture (the texture has a method for that)
	uint width, heigth;
	gTexture.GetDimensions(width, heigth);
	// Step 2: calculate dx and dy
	float dx, dy;
	dx = 1 / (float)width;
	dy = 1 / (float)heigth;
	
	// Step 3: Create a double for loop (5 iterations each)
	//		   Inside the look, calculate the offset in each direction. Make sure not to take every pixel but move by 2 pixels each time
	//			Do a texture lookup using your previously calculated uv coordinates + the offset, and add to the final color
	
	for (int i = -5; i < 5; i+=2)
	{
		for (int ii = -5; ii < 5; ii += 2)
		{
			color += gTexture.Sample(samLinear, input.Tex + float2(i*dx, ii*dy));
		}
	}

	color = color / 25.0f;
	// Step 4: Divide the final color by the number of passes (in this case 5*5)
	
	// Step 5: return the final color

	return float4(color, colorOpacity);
}

technique11 Blur
{
    pass P0
    {
		// Set states
		SetRasterizerState(backCull);
		SetDepthStencilState(EnableDepth, 0);
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}