//************
// VARIABLES *
//************

float m_LeafSize : LeafSize;
float2 m_TreeSize : TreeSize;
float4x4 m_MatrixWorldViewProj : WORLDVIEWPROJECTION;
float4x4 m_MatrixWorld : WORLD;
float3 m_LightDir = float3(-0.577f, -0.577f, 0.577f);

Texture2D m_TextureTree : TreeTexture;
Texture2D m_TextureLeaf : LeafTexture;
Texture2D m_OpacityLeaf : OpacityMap;

BlendState EnableBlending
{
	BlendEnable[0] = TRUE;
	SrcBlend = SRC_ALPHA;
	DestBlend = INV_SRC_ALPHA;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

RasterizerState FrontCulling 
{ 
	CullMode = FRONT; 
};

RasterizerState NoCulling
{
	CullMode = NONE;
};

DepthStencilState DisableDepthWriting
{
	DepthEnable = TRUE;
	DepthWriteMask =ZERO;
};

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

SamplerState samLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};


//**********
// STRUCTS *
//**********

struct VS_DATA
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
    float2 TexCoord : TEXCOORD;
};

struct GS_DATA
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
};

//first pass without geometry so not double sided
//

GS_DATA VS_Tree(VS_DATA input){
	GS_DATA output;
	output.Position = mul ( float4(input.Position,1.0f), m_MatrixWorldViewProj );
	output.Normal = normalize(mul(input.Normal, (float3x3)m_MatrixWorld));
	output.TexCoord = input.TexCoord;
	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS_Tree(GS_DATA input) : SV_TARGET{

	float4 diffuseColor = m_TextureTree.Sample( samLinear,input.TexCoord );
	float3 color_rgb= diffuseColor.rgb;
	float color_a = diffuseColor.a;
	
	//half lambrt difuse
	float diffuseStrength = dot(input.Normal, -m_LightDir);
	diffuseStrength = diffuseStrength * 0.5 + 0.5;
	diffuseStrength = saturate(diffuseStrength);
	color_rgb = color_rgb * diffuseStrength;

	return float4( color_rgb , color_a );
}


//second pass for geometry shader, double sided

//****************
// VERTEX SHADER *
//****************
VS_DATA VS_Leaf(VS_DATA vsData)
{
	return vsData;
}

//calculate opacity based on the opacity map
float CalculateOpacity(float2 texCoord)
{
	float opacity = m_OpacityLeaf.Sample(samLinear, texCoord).r;
	return opacity;
}

//******************
// GEOMETRY SHADER *
//******************

void CreateVertex(inout TriangleStream<GS_DATA> triStream, float3 pos, float3 normal, float2 texCoord)
{
	GS_DATA temp = (GS_DATA)0;
	temp.Position = mul(float4(pos,1.0f),m_MatrixWorldViewProj);
	temp.Normal = mul(normal,(float3x3)m_MatrixWorld);

	temp.TexCoord = texCoord;
	
	triStream.Append(temp);
}

void CreateLeaf(inout TriangleStream<GS_DATA> triStream,float3 botRight, float3 botLeft, float3 topRight, float3 topLeft)
{
	float3 leafNormal =cross((botLeft-topRight),(botRight-topRight));
	CreateVertex(triStream, botRight, leafNormal, float2(1,1));
	CreateVertex(triStream, topRight, leafNormal, float2(1,0));
	CreateVertex(triStream, topLeft, leafNormal, float2(0,0));
		
	triStream.RestartStrip();
	
	CreateVertex(triStream, topLeft, leafNormal, float2(0,0));
	CreateVertex(triStream, botLeft, leafNormal, float2(0,1));
	CreateVertex(triStream, botRight, leafNormal, float2(1,1));
	
	triStream.RestartStrip();
}

//randomize algorhtim based on a vector2 seed
float random( float2 p )
{
  const float2 r = float2(
    23.1406926327792690,  // e^pi (Gelfond's constant)
     2.6651441426902251); // 2^sqrt(2) (Gelfond–Schneider constant)
  return frac( cos( fmod( 123456789., 1e-7 + 256. * dot(p,r) ) ) );  
}

//creates a quad for each pass, does not push the vertices of the old mesh out
[maxvertexcount(60)]
void LeafGenerator(line VS_DATA vertices[2], inout TriangleStream<GS_DATA> triStream)
{

	float3 topLeft, topRight, botLeft, botRight, leafHeigthDirection, leafDirection, leafNormal;

	
	//checks if leave is either to low or to close to center
	if (vertices[0].Position.y < m_TreeSize.y * 0.25f)
		return;	
		
	if( length(vertices[0].Position.xz) < m_TreeSize.x * 0.25f && vertices[0].Position.y < m_TreeSize.y * 0.75f )
		return;	
	
	//calculate the density of the leafs, the furthe from the center the denser
	float leafDensity = length(vertices[0].Position.xz);
	
	//gets the position in world coordinates
	float4 tempPosition = mul(float4(vertices[0].Position,1.0f),m_MatrixWorld);
	
	leafHeigthDirection = normalize((vertices[0].Normal + vertices[1].Normal) 
	+ ((float3(random(tempPosition.xz),random(tempPosition.xz),random(tempPosition.xz))*2-1))) ;
	
	leafDirection = normalize(vertices[0].Position - vertices[1].Position);
	
	

		botRight = vertices[1].Position + leafDirection * m_LeafSize; 
		botLeft = botRight + leafDirection* m_LeafSize * random(vertices[0].TexCoord);
	
		topLeft = botLeft + leafHeigthDirection * m_LeafSize * random(vertices[0].TexCoord);
		topRight = botRight + leafHeigthDirection * m_LeafSize * random(vertices[0].TexCoord);
			
		CreateLeaf(triStream,botRight,botLeft,topRight,topLeft);
	
	


}


//***************
// PIXEL SHADER *
//***************
float4 PS_Leaf(GS_DATA input) : SV_TARGET 
{
	input.Normal=-normalize(input.Normal);
	
	float alpha = 0.f;
	float3 color = 1.f;
	
	alpha = CalculateOpacity(input.TexCoord);
	color = m_TextureLeaf.Sample( samLinear,input.TexCoord ).rgb;
	//HalfLambert Diffuse 
	float diffuseStrength = dot(input.Normal, -m_LightDir);
	diffuseStrength = diffuseStrength * 0.5 + 0.5;
	diffuseStrength = saturate(diffuseStrength);
	color = color * diffuseStrength;

	return float4(color, alpha);
}


//*************
// TECHNIQUES *
//*************
technique10 Default
{
	pass P0
   {
		SetRasterizerState(FrontCulling);
		SetDepthStencilState(EnableDepth, 0);
		SetVertexShader( CompileShader( vs_4_0, VS_Tree() ) );
		SetGeometryShader( NULL );
		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetPixelShader( CompileShader( ps_4_0, PS_Tree() ) );
   }
	
	pass p1 {
		SetRasterizerState(NoCulling);	
		SetVertexShader(CompileShader(vs_4_0, VS_Leaf()));
		SetGeometryShader(CompileShader(gs_4_0, LeafGenerator()));
		SetPixelShader(CompileShader(ps_4_0, PS_Leaf()));
		SetBlendState(EnableBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetDepthStencilState(DisableDepthWriting, 0);
	}
	
}