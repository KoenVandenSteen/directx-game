#include "Base\stdafx.h"

#include "HumanSiegeShop.h"
#include "Components\Components.h"
#include "../Units/UnitManager.h"

const int HumanSiegeShop::m_buildingCost = 400;

HumanSiegeShop::HumanSiegeShop(ModelConstruction buildingConstruction)
{
	m_BuildingConstruction = buildingConstruction;
	m_RespawnTime = 24.0f;
	m_HitPoints = 0;
	m_RespawnTimer = 0;
}

HumanSiegeShop::~HumanSiegeShop(){

}



void HumanSiegeShop::Update(const GameContext& gameContext){

	UNREFERENCED_PARAMETER(gameContext);

	auto unitManager = UnitManager::GetInstance();
	if (m_RespawnTimer > m_RespawnTime){
		if (m_isPlayerBuilding)
			unitManager->AddFriendlyUnit(UnitManager::UnitType::SiegeType, XMFLOAT3(GetParent()->GetTransform()->GetPosition().x, GetParent()->GetTransform()->GetPosition().y + 10.f, GetParent()->GetTransform()->GetPosition().z + 20.f));
		else
			unitManager->AddEnemyUnit(UnitManager::UnitType::SiegeType, XMFLOAT3(GetParent()->GetTransform()->GetPosition().x, GetParent()->GetTransform()->GetPosition().y + 10.f, GetParent()->GetTransform()->GetPosition().z - 20.f));

		m_RespawnTimer = 0;
	}
	m_RespawnTimer += gameContext.pGameTime->GetElapsed();

}

