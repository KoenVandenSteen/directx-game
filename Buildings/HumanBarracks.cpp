#include "Base\stdafx.h"

#include "HumanBarracks.h"
#include "../Units/UnitManager.h"
#include "Components\Components.h"

const int HumanBarracks::m_buildingCost = 50;

HumanBarracks::HumanBarracks(ModelConstruction buildingConstruction)
{
	m_BuildingConstruction = buildingConstruction;
	m_RespawnTime = 12.0f;
	m_RespawnTimer = 0;
}

HumanBarracks::~HumanBarracks(){

}



void HumanBarracks::Update(const GameContext& gameContext){

	UNREFERENCED_PARAMETER(gameContext);
	auto unitManager = UnitManager::GetInstance();
	
	if (m_RespawnTimer > m_RespawnTime){
		if (m_isPlayerBuilding)
			unitManager->AddFriendlyUnit(UnitManager::UnitType::SoldierType, XMFLOAT3(GetParent()->GetTransform()->GetPosition().x, GetParent()->GetTransform()->GetPosition().y + 10.f, GetParent()->GetTransform()->GetPosition().z + 20.f));
		else
			unitManager->AddEnemyUnit(UnitManager::UnitType::SoldierType, XMFLOAT3(GetParent()->GetTransform()->GetPosition().x, GetParent()->GetTransform()->GetPosition().y + 10.f, GetParent()->GetTransform()->GetPosition().z - 20.f));
	
		m_RespawnTimer = 0;
	}
	
	
	m_RespawnTimer += gameContext.pGameTime->GetElapsed();
}

