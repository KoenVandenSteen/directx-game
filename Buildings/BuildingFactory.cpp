#include "Base\stdafx.h"
#include "BuildingFactory.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
#include "Content/ContentManager.h"
#include "HumanBarracks.h"
#include "HumanRange.h"
#include "HumanSiegeShop.h"
#include "HumanStables.h"


BuildingFactory *BuildingFactory::m_pBuildingFactory = nullptr;

BuildingFactory::BuildingFactory()
{
	m_vModelContruction.resize (BuildingList::COUNT);
}


BuildingFactory::~BuildingFactory()
{
}

void BuildingFactory::Initialize(const GameContext& gameContext)
{
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	PxMaterial * defMat = physX->createMaterial(1.0f, 1.0f, 0.f);
	m_vModelContruction.at(BuildingList::HumanBarracksNum) = CreateModelConstruction(L"./TheGame/Meshes/Barracks.ovm", L"./TheGame/Meshes/Barracks.ovpc", L"./TheGame/Textures/DefaultTexture.dds", BuildingList::HumanBarracksNum, defMat, gameContext, 4.f);
	m_vModelContruction.at(BuildingList::HumanRangeNum) = CreateModelConstruction(L"./TheGame/Meshes/Range.ovm", L"./TheGame/Meshes/Range.ovpc", L"./TheGame/Textures/DefaultTexture.dds", BuildingList::HumanRangeNum, defMat, gameContext, 0.05f);
	m_vModelContruction.at(BuildingList::HumanStablesNum) = CreateModelConstruction(L"./TheGame/Meshes/Stables.ovm", L"./TheGame/Meshes/Stables.ovpc", L"./TheGame/Textures/DefaultTexture.dds", BuildingList::HumanStablesNum, defMat, gameContext, 0.05f);
	m_vModelContruction.at(BuildingList::HumanSiegeShopNum) = CreateModelConstruction(L"./TheGame/Meshes/SiegeShop.ovm", L"./TheGame/Meshes/SiegeShop.ovpc", L"./TheGame/Textures/DefaultTexture.dds", BuildingList::HumanSiegeShopNum, defMat, gameContext, 0.07f);
}


ModelConstruction BuildingFactory::CreateModelConstruction(const wstring meshPath, const  wstring convexPath, const wstring texturePath, const  UINT matID, PxMaterial *defaultMat, const GameContext& gameContext, const PxReal scale){
	
	ModelConstruction returnConstruction;

	returnConstruction.mesh = ContentManager::Load<MeshFilter>(meshPath);
	returnConstruction.convexMesh = ContentManager::Load<PxConvexMesh>(convexPath);

	DiffuseMaterial *difMat = new DiffuseMaterial(false);
	difMat->SetDiffuseTexture(texturePath);
	gameContext.pMaterialManager->AddMaterial(difMat, matID);
	returnConstruction.pxMaterial = defaultMat;
	returnConstruction.Scale = scale;

	return returnConstruction;
}

Building *BuildingFactory::CreateHumanBarracks(){
	return new HumanBarracks(m_vModelContruction.at(BuildingList::HumanBarracksNum));
}

Building *BuildingFactory::CreateHumanSiegeShop(){
	return new HumanSiegeShop(m_vModelContruction.at(BuildingList::HumanSiegeShopNum));
}
Building *BuildingFactory::CreateHumanStables(){
	return new HumanStables(m_vModelContruction.at(BuildingList::HumanStablesNum));
}
Building *BuildingFactory::CreateHumanRange(){
	return new HumanRange(m_vModelContruction.at(BuildingList::HumanRangeNum));
}

 const int BuildingFactory::ReturnBuildingCost(BuildingList curBuilding){

	switch (curBuilding)
	{
	case BuildingFactory::HumanBarracksNum:
		return HumanBarracks::ReturnBuildingCost();
		break;
	case BuildingFactory::HumanRangeNum:
		return HumanRange::ReturnBuildingCost();
		break;
	case BuildingFactory::HumanStablesNum:
		return HumanStables::ReturnBuildingCost();
		break;
	case BuildingFactory::HumanSiegeShopNum:
		return HumanSiegeShop::ReturnBuildingCost();
		break;
	default:
		break;
	}

	return 0;
}

