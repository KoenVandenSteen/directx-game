#pragma once
#include "Building.h"

class Unit;

class HumanBarracks : public Building 
{
public:
	HumanBarracks(ModelConstruction buildingConstruction);
	~HumanBarracks();

	
	virtual void Update(const GameContext& gameContext);
	static const int ReturnBuildingCost(){ return m_buildingCost; };
private:

	float m_RespawnTimer;
	static const int m_buildingCost;

};

