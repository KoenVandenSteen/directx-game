#pragma once

#include "Building.h"

class BuildingFactory : public GameObject
{
public:


	BuildingFactory();
	~BuildingFactory();

	enum BuildingList
	{
		HumanBarracksNum = 0,
		HumanRangeNum,
		HumanStablesNum,
		HumanSiegeShopNum,
		COUNT
	};

	
	void Initialize(const GameContext& gameContext);
	Building *CreateHumanBarracks();
	Building *CreateHumanSiegeShop();
	Building *CreateHumanStables();
	Building *CreateHumanRange();

	const int ReturnBuildingCost(BuildingList curBuilding);
	void Reset();

	static BuildingFactory *GetInstance(){ return m_pBuildingFactory ? m_pBuildingFactory : m_pBuildingFactory = new BuildingFactory(); };

private: 

	ModelConstruction CreateModelConstruction(const wstring meshPath, const wstring convexPath, const wstring texturePath, const UINT mathID, PxMaterial *defaultMat, const GameContext& gameContext, const PxReal scale);
	static BuildingFactory* m_pBuildingFactory;
	vector<ModelConstruction>  m_vModelContruction;

};
