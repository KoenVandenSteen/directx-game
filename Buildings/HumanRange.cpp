#include "Base\stdafx.h"

#include "HumanRange.h"
#include "Components\Components.h"
#include "../Units/UnitManager.h"


const int HumanRange::m_buildingCost = 200;

HumanRange::HumanRange(ModelConstruction buildingConstruction)
{
	m_BuildingConstruction = buildingConstruction;
	m_RespawnTime = 12.0f;
	m_HitPoints = 0;
	m_RespawnTimer = 0;
}

HumanRange::~HumanRange(){
	
}



void HumanRange::Update(const GameContext& gameContext){

	UNREFERENCED_PARAMETER(gameContext);

	UNREFERENCED_PARAMETER(gameContext);
	auto unitManager = UnitManager::GetInstance();

	if (m_RespawnTimer > m_RespawnTime){
		if (m_isPlayerBuilding)
			unitManager->AddFriendlyUnit(UnitManager::UnitType::ArcherType, XMFLOAT3(GetParent()->GetTransform()->GetPosition().x, GetParent()->GetTransform()->GetPosition().y + 10.f, GetParent()->GetTransform()->GetPosition().z + 20.f));
		else
			unitManager->AddEnemyUnit(UnitManager::UnitType::ArcherType, XMFLOAT3(GetParent()->GetTransform()->GetPosition().x, GetParent()->GetTransform()->GetPosition().y + 10.f, GetParent()->GetTransform()->GetPosition().z - 20.f));

		m_RespawnTimer = 0;
	}

	m_RespawnTimer += gameContext.pGameTime->GetElapsed();

}

//Unit *HumanRange::CreateUnit(){
//	return new Unit;
//}