#pragma once
#include "..\OverlordEngine\Scenegraph\GameObject.h"
#include "Components\ModelComponent.h"
#include "../../Materials/DiffuseMaterial.h"

class Unit;

struct ModelConstruction{
	MeshFilter *mesh = nullptr;
	PxConvexMesh *convexMesh = nullptr;
	UINT matID = 0;
	PxMaterial *pxMaterial = nullptr;
	PxReal Scale = 0;
};


class Building : public GameObject
{
public:
	Building();
	~Building();
	
	void SetPlayerBuilding(bool isPLayerBuilding){ m_isPlayerBuilding = isPLayerBuilding; };
	int GetHitPoints(){ return m_HitPoints; }

protected:
	void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext){
		UNREFERENCED_PARAMETER(gameContext);
	}

	float m_RespawnTime = 0;
	int m_HitPoints;
	ModelConstruction m_BuildingConstruction;
	bool m_isPlayerBuilding;
	
private:

};

