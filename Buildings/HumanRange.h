#pragma once
#include "Building.h"

class Unit;

class HumanRange : public Building 
{
public:
	HumanRange(ModelConstruction buildingConstruction);
	~HumanRange();

	
	void HumanRange::Update(const GameContext& gameContext);
	static const int ReturnBuildingCost(){ return m_buildingCost; };
private:
	//Unit *CreateUnit();

	float m_RespawnTimer;
	static const int m_buildingCost;
};

