#pragma once
#include "Building.h"

class Unit;

class HumanStables : public Building 
{
public:
	HumanStables(ModelConstruction buildingConstruction);
	~HumanStables();

	
	void HumanStables::Update(const GameContext& gameContext);
	static const int ReturnBuildingCost(){ return m_buildingCost; };
private:
	//Unit *CreateUnit();
	static const int m_buildingCost;
	float m_RespawnTimer;
};

