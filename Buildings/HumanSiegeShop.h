#pragma once
#include "Building.h"

class Unit;

class HumanSiegeShop : public Building 
{
public:
	HumanSiegeShop(ModelConstruction buildingConstruction);
	~HumanSiegeShop();

	
	void HumanSiegeShop::Update(const GameContext& gameContext);
	static const int ReturnBuildingCost(){ return m_buildingCost; };
private:
	//Unit *CreateUnit();

	float m_RespawnTimer;
	static const int m_buildingCost;
};

