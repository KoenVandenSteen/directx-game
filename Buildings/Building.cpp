#include "Base\stdafx.h"

#include "Building.h"
#include "Components/Components.h"
#include "Content/ContentManager.h"



Building::Building()
{

}

Building::~Building(){

}


void Building::Initialize(const GameContext& gameContext) //sets the right texture and mesh
{
	UNREFERENCED_PARAMETER(gameContext);

	auto model = new ModelComponent(m_BuildingConstruction.mesh);
	model->SetMaterial(m_BuildingConstruction.matID);

	if (GetTransform()->GetPosition().z != 0){
		auto rigidbody = new RigidBodyComponent(true);
		AddComponent(rigidbody);
		std::shared_ptr<PxGeometry> meshGeom(new PxConvexMeshGeometry(m_BuildingConstruction.convexMesh, PxMeshScale(m_BuildingConstruction.Scale)));
		AddComponent(new ColliderComponent(meshGeom, *m_BuildingConstruction.pxMaterial, PxTransform(PxVec3(0, 0, 0))));
	}

	AddComponent(model);
}

