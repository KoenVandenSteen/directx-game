//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
#pragma once
#include "Scenegraph/GameScene.h"

//class BuildingFactory;
class World;
class HeroCharacter;
class GoldPickUp;
class Interface;

class TheGameScene : public GameScene
{
public:
	TheGameScene(void);
	virtual ~TheGameScene(void);

	enum HeroState
	{
		BuildStage,
		CoinStage,
	};

	enum GameState
	{
		Start,
		Play,
		End
	};
	
	enum PostProcessingDraw
	{
		Blur = 51,
		Grey,
		Sepia
	};
	HeroState GetCurrentHeroState(){
		return m_currentHeroState;
	}

	GameState GetCurrentGameState(){
		return m_currentGameState;
	}


	void DealDamageToPlayerBase(float damage){ m_PlayerBaseHealth -= damage; };
	void DealDamageToEnemyBase(float damage){ m_EnemyBaseHealth -= damage; };

	void SetCurrentHeroState(HeroState stateToSet);
	void SetCurrentGameState(GameState gameState);

	void GoldPickedUp(float ammount);

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

private:

	void ResetGame();
	void SpawnGoldStageEvent();
	void MovingWallEvent(float time);
	void CheckCoins();
	void CheckCameraRay(const GameContext& gameContext);
	HeroState m_currentHeroState;
	GameState m_currentGameState = GameState::Start;
	bool firsthit = true;
	World *m_pWorld = nullptr;
	HeroCharacter *m_pHero = nullptr;
	bool m_EnteredGoldStage = true;
	bool m_ZoomIn = false;
	const UINT m_ammountOfCoins;
	const float m_coinStartValue;
	const float m_respawnWall;
	float m_wallRespawnTimer;
	Interface *m_pInterface = nullptr;
	vector<GoldPickUp*> m_vGoldPickups;
	void ResetHero();
	void AddParticleEffects();
	float m_CoinMultiplier = 1;

	float m_PlayerBaseHealth;
	float m_EnemyBaseHealth;
	float m_StartingHealth;
	float m_StartingGold;

	PostProcessingMaterial *m_pBlurMat = nullptr, *m_pGreyMat = nullptr, *m_pSepiaMat = nullptr;
	
	//BuildingFactory * m_pBuildingFactory = nullptr;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	TheGameScene( const TheGameScene &obj);
	TheGameScene& operator=( const TheGameScene& obj);
};


