//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "ParticleEffect.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "HeroCharacter.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../TheGameScene.h"
#include "../../../OverlordEngine/Components/ParticleEmitterComponent.h"

ParticleEffect::ParticleEffect(const XMFLOAT3 velocity, const  float minSize, const  float maxSize, const  float minEnergy, const  float maxEnergy, const  float minSizeGrow, const  float maxSizeGrow, const  float minEmitterRange, const float maxEmmitterRange, const  XMFLOAT4 color, const  wstring &assetFile) :
m_curLifeTime(0),
m_velocity(velocity),
m_minSize(minSize),
m_maxSize(maxSize),
m_minEnergy(minEnergy),
m_maxEnergy(maxEnergy),
m_minSizeGrow(minSizeGrow),
m_maxSizeGrow(maxSizeGrow),
m_minEmitterRange(minEmitterRange),
m_maxEmmitterRange(maxEmmitterRange),
m_color(color),
m_assetFile(assetFile)
{
}


ParticleEffect::~ParticleEffect(void)
{
	
}

ParticleEffect::ParticleEffect(const ParticleEffect& t):
m_velocity(t.m_velocity),
m_minSize(t.m_minSize),
m_maxSize(t.m_maxSize),
m_minEnergy(t.m_minEnergy),
m_maxEnergy(t.m_maxEnergy),
m_minSizeGrow(t.m_minSizeGrow),
m_maxSizeGrow(t.m_maxSizeGrow),
m_minEmitterRange(t.m_minEmitterRange),
m_maxEmmitterRange(t.m_maxEmmitterRange),
m_color(t.m_color),
m_assetFile(t.m_assetFile)
{
}

ParticleEffect& ParticleEffect::operator=(const ParticleEffect& t)
{
	m_velocity = t.m_velocity;
	m_minSize = t.m_minSize;
	m_maxSize = t.m_maxSize;
	m_minEnergy = t.m_minEnergy;
	m_maxEnergy = t.m_maxEnergy;
	m_minSizeGrow = t.m_minSizeGrow;
	m_maxSizeGrow = t.m_maxSizeGrow;
	m_minEmitterRange = t.m_minEmitterRange;
	m_maxEmmitterRange = t.m_maxEmmitterRange;
	m_color = t.m_color;
	m_assetFile = t.m_assetFile;

	return *this;
}


void ParticleEffect::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//m_assetFile = L"./Resources/Textures/Smoke.png";
	auto particleEmitter = new ParticleEmitterComponent(m_assetFile);
	particleEmitter->SetVelocity(m_velocity);
	particleEmitter->SetMinSize(m_minSize);
	particleEmitter->SetMaxSize(m_maxSize);
	particleEmitter->SetMinEnergy(m_minEnergy);
	particleEmitter->SetMaxEnergy(m_maxEnergy);
	particleEmitter->SetMinSizeGrow(m_minSizeGrow);
	particleEmitter->SetMaxSizeGrow(m_maxSizeGrow);
	particleEmitter->SetMinEmitterRange(m_minEmitterRange);
	particleEmitter->SetMaxEmitterRange(m_maxEmmitterRange);
	particleEmitter->SetColor(m_color);
	AddComponent(particleEmitter);
}

void ParticleEffect::Update(const GameContext& gameContext)
{
	m_curLifeTime -= gameContext.pGameTime->GetElapsed();
}
