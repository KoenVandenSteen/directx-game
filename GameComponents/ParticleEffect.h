#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;

class ParticleEffect : public GameObject
{
public:
	ParticleEffect(const XMFLOAT3 velocity, const  float minSize, const  float maxSize, const  float minEnergy, const  float maxEnergy, const  float minSizeGrow, const  float maxSizeGrow, const  float minEmitterRange, const  float maxEmmitterRange, const  XMFLOAT4 color, const  wstring &assetFile);
	virtual ~ParticleEffect();


	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	bool IsDead(){ return m_curLifeTime < 0 ? true : false; };
	void SetDuration(float duration){ m_curLifeTime = duration; };

	ParticleEffect(const ParticleEffect& t);
	ParticleEffect& operator=(const ParticleEffect& t);

private:

	float m_curLifeTime;
	XMFLOAT3 m_velocity;
	float m_minSize;
	float m_maxSize;
	float m_minEnergy;
	float m_maxEnergy;
	float m_minSizeGrow;
	float m_maxSizeGrow;
	float m_minEmitterRange;
	float m_maxEmmitterRange;
	XMFLOAT4 m_color;
	wstring m_assetFile;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	//;
	
};

