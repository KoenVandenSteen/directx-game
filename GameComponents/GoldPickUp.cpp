//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "GoldPickUp.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../../Materials/UberMaterial.h"
#include "Content/ContentManager.h"
#include "GameWall.h"
#include "../TheGameScene.h"
#include "HeroCharacter.h"
GoldPickUp::GoldPickUp(float value, int materialID) :
m_value(value),
m_materialID(materialID)
{
}


GoldPickUp::~GoldPickUp(void)
{
}

void GoldPickUp::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//lEVEL CREATION
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto defaultMaterial = physX->createMaterial(0.5, 0.5, 1);
	
	auto levelMesh = new ModelComponent(L"./TheGame/Meshes/GoldPickupMesh.ovm");

	AddComponent(levelMesh);


	
	
	levelMesh->SetMaterial(m_materialID);

	

	auto rigidbody = new RigidBodyComponent();
	rigidbody->SetKinematic(true);

	this->SetOnTriggerCallBack([this](GameObject* trigger, GameObject* receiver, TriggerAction action)
	{
		UNREFERENCED_PARAMETER(receiver);
		UNREFERENCED_PARAMETER(trigger);
		if (action == TriggerAction::ENTER && dynamic_cast<HeroCharacter*>(receiver)){
			m_isPickedUp = true;
			static_cast<TheGameScene*>(GetScene())->GoldPickedUp(10.0f);
		}
	});

	AddComponent(rigidbody);
	std::shared_ptr<PxGeometry> boxGeometry(new PxConvexMeshGeometry(ContentManager::Load<PxConvexMesh>(L"./TheGame/Meshes/GoldPickupMesh.ovpc"),PxMeshScale(0.2f)));
	auto collider = new ColliderComponent(boxGeometry, *defaultMaterial, PxTransform::createIdentity());
	collider->EnableTrigger(true);
	AddComponent(collider);

	GetTransform()->Scale(0.2f, 0.2f, 0.2f);
}


void GoldPickUp::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	GetTransform()->Rotate(90, 0, 0);

}
void GoldPickUp::Update(const GameContext& gamecontext){
	UNREFERENCED_PARAMETER(gamecontext);
	m_Rotation+=gamecontext.pGameTime->GetElapsed()*60.0F;
	GetTransform()->Rotate(90, m_Rotation, 0);

}