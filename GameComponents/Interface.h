#pragma once
#include "Scenegraph/GameObject.h"

class SpriteFont;
class TheGameScene;

class Interface : public GameObject
{
public:
	Interface();
	virtual ~Interface();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void PostInitialize(const GameContext& gameContext);
	void Draw(const GameContext& gameContext);

	void SetGold(int ammount){ m_gold = ammount; }
	void SetBaseHitPoints(int ammount) { m_baseHitPoints = ammount; }
	void SetEnemenyBaseHitPoints(int ammount){ m_enemyBaseHitPoints = ammount; };

	void MoveSelectorToLeft(const GameContext& gameContext);
	void MoveSelectorToRight(const GameContext& gameContext);

	bool IsButtonBusy(){ return m_MovingLeft || m_MovingRight; }
	
private:

	SpriteFont *m_pSpriteFont = nullptr;
	GameObject *m_pMenu = nullptr;
	GameObject *m_selectButton = nullptr;
	TheGameScene *m_pGameScene = nullptr;
	int m_gold = 0;
	int m_baseHitPoints = 0;
	int m_enemyBaseHitPoints = 0;
	float m_buttonSpeed;
	float m_buttonOffset;
	XMFLOAT3 m_startPos;
	XMFLOAT3 m_endPos;
	bool m_MovingLeft = false, m_MovingRight = false;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Interface(const Interface& t);
	Interface& operator=(const Interface& t);
};

