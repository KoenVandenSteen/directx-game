#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;

class GoldPickUp : public GameObject
{
public:
	GoldPickUp(float value, int materialID);
	virtual ~GoldPickUp();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void PostInitialize(const GameContext& gameContext);
	bool IsPickedUp(){ return m_isPickedUp; };
	void SetPickedUp(bool pickup){ m_isPickedUp = pickup; };
private:

	bool m_isPickedUp = false;
	float m_value;
	int m_materialID;
	float m_Rotation = 0;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	GoldPickUp(const GoldPickUp& t);
	GoldPickUp& operator=(const GoldPickUp& t);
};

