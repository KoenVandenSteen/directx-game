//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "ParticleManager.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "HeroCharacter.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../TheGameScene.h"
#include "ParticleEffect.h"

ParticleManager* ParticleManager::m_pParticleManager = nullptr;

ParticleManager::ParticleManager()
{
}

ParticleManager::~ParticleManager(void)
{
	for (auto &it : m_ParticleEffects)
	{
		SafeDelete(it.second);
	}


}

void ParticleManager::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void ParticleManager::Update(const GameContext& gameContext)
{

	UNREFERENCED_PARAMETER(gameContext);

	for (UINT i = 0; i < m_currParticleEffects.size(); ++i)
	{
		if (m_currParticleEffects.at(i) != nullptr && m_currParticleEffects.at(i)->IsDead())
		{
			RemoveChild(m_currParticleEffects[i]);
			m_currParticleEffects.erase(m_currParticleEffects.begin() + i);
			break;
		}
	}
}

void ParticleManager::AddParticleEffect(const string name,const XMFLOAT3 velocity, const  float minSize, const  float maxSize, const  float minEnergy, const  float maxEnergy, const  float minSizeGrow, const  float maxSizeGrow, const  float minEmitterRange, const float maxEmmitterRange, const  XMFLOAT4 color, const  wstring &assetFile)
{
	auto particleEffect = new ParticleEffect(velocity, minSize, maxSize, minEnergy, maxEnergy, minSizeGrow, maxSizeGrow, minEmitterRange, maxEmmitterRange,color,assetFile);
	m_ParticleEffects[name] = particleEffect;
}

void ParticleManager::SpawnParticleEffect(XMFLOAT3 position, float duration, string name){
	
	ParticleEffect * particleEffect = new ParticleEffect(*m_ParticleEffects[name]);

	AddChild(particleEffect);
	
	particleEffect->SetDuration(duration);
	particleEffect->GetTransform()->Translate(position);
		
	m_currParticleEffects.push_back(particleEffect);
}
