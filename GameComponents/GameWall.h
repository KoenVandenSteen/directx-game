#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;

class GameWall : public GameObject
{
public:
	GameWall(float width, float height, float depth, bool isTrigger = false);
	virtual ~GameWall();

	virtual void Initialize(const GameContext& gameContext);
	
	bool IsTriggered() const { return m_WasTriggered; }
	void Reset(){ m_WasTriggered = false; }

private:

	XMFLOAT3 m_Dimensions;
	bool m_IsTrigger;
	bool m_WasTriggered;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	GameWall(const GameWall& t);
	GameWall& operator=(const GameWall& t);
};

