#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;

class MovingWall : public GameObject
{
public:
	MovingWall(UINT matIndex);
	virtual ~MovingWall();

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);


private:


	float m_walSpeed;
	UINT m_matIndex;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	MovingWall(const MovingWall& t);
	MovingWall& operator=(const MovingWall& t);
};

