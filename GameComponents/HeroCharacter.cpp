//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "HeroCharacter.h"
#include "Components\Components.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameScene.h"
#include "Physx/PhysxManager.h"
#include "Physx/PhysxProxy.h"
#include "Diagnostics/Logger.h"
#include "../Buildings/BuildingFactory.h"
#include "..\..\Materials\SkinnedDiffuseMaterial.h"
#include "..\..\Materials\DiffuseMaterial.h"
#include "Graphics\ModelAnimator.h"
#include "Interface.h"
#include "BuildingPlot.h"

#include "..\Buildings\HumanBarracks.h"
#include "..\Buildings\HumanRange.h"
#include "..\Buildings\HumanSiegeShop.h"
#include "..\Buildings\HumanStables.h"


HeroCharacter::HeroCharacter(float radius, float height, float moveSpeed) : 
	m_Radius(radius),
	m_Height(height),
	m_MoveSpeed(moveSpeed),
	m_pCamera(nullptr),
	m_pController(nullptr),
	m_TotalPitch(0), 
	m_TotalYaw(0),
	m_RotationSpeed(90.f),
	//Running
	m_MaxRunVelocity(50.0f), 
	m_TerminalVelocity(1),
	m_Gravity(9.81f), 
	m_RunAccelerationTime(0.3f), 
	m_JumpAccelerationTime(0.8f), 
	m_RunAcceleration(m_MaxRunVelocity/m_RunAccelerationTime), 
	m_JumpAcceleration(m_Gravity/m_JumpAccelerationTime), 
	m_RunVelocity(0), 
	m_JumpVelocity(0),
	m_Velocity(0,0,0),
	m_StartMoney(20000),
	m_minCamDistance(10.f),
	m_maxCamDistance(30.f),
	m_CamMoveSpeed(100.f)
{
}

HeroCharacter::~HeroCharacter(void)
{

}

void HeroCharacter::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Create controller
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto mat = physX->createMaterial(0, 0, 1);
	auto controller = new ControllerComponent(mat, m_Radius, m_Height, L"HeroCharacter", PxCapsuleClimbingMode::eEASY);
	//auto rigiBody = new RigidBodyComponent(false);
	this->AddComponent(controller);
	// Add a fixed camera as child


	auto fixedCamera = new FixedCamera();
	this->AddChild(fixedCamera);
	// Register all Input Actions
	auto inputAction = InputAction(HeroCharacterMovement::BACKWARD, InputTriggerState::Down, 'S');
	gameContext.pInput->AddInputAction(inputAction);
	inputAction = InputAction(HeroCharacterMovement::FORWARD, InputTriggerState::Down, 'W');
	gameContext.pInput->AddInputAction(inputAction);
	inputAction = InputAction(HeroCharacterMovement::LEFT, InputTriggerState::Down, 'A');
	gameContext.pInput->AddInputAction(inputAction);
	inputAction = InputAction(HeroCharacterMovement::RIGHT, InputTriggerState::Down, 'D');
	gameContext.pInput->AddInputAction(inputAction);
	inputAction = InputAction(HeroCharacterMovement::BUILD, InputTriggerState::Pressed, VK_SPACE);
	gameContext.pInput->AddInputAction(inputAction);
	inputAction = InputAction(HeroCharacterMovement::SELECTLEFTBUILDING, InputTriggerState::Pressed, VK_LEFT);
	gameContext.pInput->AddInputAction(inputAction);
	inputAction = InputAction(HeroCharacterMovement::SELECTRIGHTBUILDING, InputTriggerState::Pressed, VK_RIGHT);
	gameContext.pInput->AddInputAction(inputAction);

	m_pBuildingFactory = BuildingFactory::GetInstance();


	//hero mesh
	auto skinnedDiffuseMaterial = new SkinnedDiffuseMaterial();
	skinnedDiffuseMaterial->SetDiffuseTexture(L"./TheGame/Textures/HeroTexture.dds");
	gameContext.pMaterialManager->AddMaterial(skinnedDiffuseMaterial, 55);

	m_pModel = new ModelComponent(L"./TheGame/Meshes/HeroMesh.ovm");
	m_pModel->SetMaterial(55);
	auto obj = new GameObject();
	obj->AddComponent(m_pModel);
	AddChild(obj);
	obj->GetTransform()->Scale(0.05f, 0.05f, 0.05f);
	obj->GetTransform()->Translate(0.f, -5.f, 0.f);
	obj->GetTransform()->Rotate(0.f, 180.f, 0.f);

	m_pInterface = new Interface();
	GetScene()->AddChild(m_pInterface);

	m_currentAmmountOfGold = m_StartMoney;
}

void HeroCharacter::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	// Set the camera as active
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
	
	m_pCamera = GetChild<FixedCamera>()->GetComponent<CameraComponent>();
	m_pCamera->GetTransform()->Rotate(20,0,0);
	GetScene()->SetActiveCamera(m_pCamera);
}


void HeroCharacter::Update(const GameContext& gameContext)
{
	
	auto animator = m_pModel->GetAnimator();
	
	if (!animator->IsPlaying()){
		animator->SetAnimation(L"Idle");
		animator->Play();
	}
	//Update the HeroCharacter (Camera rotation, HeroCharacter Movement, HeroCharacter Gravity)
	/*Movement*/
	///////////////////////////////


	float look = 0;
	if (gameContext.pInput->IsActionTriggered(HeroCharacterMovement::LEFT)){
		look = -1;
	}
		
	if (gameContext.pInput->IsActionTriggered(HeroCharacterMovement::RIGHT)){
		look = 1;
	}

	m_TotalYaw += look * m_RotationSpeed * gameContext.pGameTime->GetElapsed();

	XMVECTOR forward = XMLoadFloat3(&GetTransform()->GetForward());
	

	if (gameContext.pInput->IsActionTriggered(HeroCharacterMovement::FORWARD) && animator->GetClipName() != L"Attack"){
		if (m_RunVelocity < m_MaxRunVelocity)
			m_RunVelocity += m_RunAcceleration * gameContext.pGameTime->GetElapsed();
		if (animator->GetClipName() != L"Run")
			animator->SetAnimation(L"Run");

	}
	else
	{
		if (m_RunVelocity > 0)
			m_RunVelocity -= m_RunAcceleration * gameContext.pGameTime->GetElapsed();
	}

	if (gameContext.pInput->IsActionTriggered(HeroCharacterMovement::BACKWARD) && animator->GetClipName() != L"Attack")
	{
		if (m_RunVelocity > -m_MaxRunVelocity)
			m_RunVelocity -= m_RunAcceleration * gameContext.pGameTime->GetElapsed();
		if (animator->GetClipName() != L"Run")
			animator->SetAnimation(L"Run");
	}
	else
	{
		if (m_RunVelocity < 0)
			m_RunVelocity += m_RunAcceleration * gameContext.pGameTime->GetElapsed();
	}

	float temp = m_Velocity.y;

	XMVECTOR velocityVector = XMLoadFloat3(&m_Velocity);
	velocityVector = m_RunVelocity*forward;
	XMStoreFloat3(&m_Velocity, velocityVector);

	m_Velocity.y = temp;

	//cout << "x. " << GetTransform()->GetPosition().x << " Y. " << GetTransform()->GetPosition().y << " Z. " <<GetTransform()->GetPosition().z << endl;

	if (this->GetComponent<ControllerComponent>()->GetCollisionFlags() != PxControllerCollisionFlag::eCOLLISION_DOWN)
	{
			m_JumpVelocity -= m_JumpAcceleration*gameContext.pGameTime->GetElapsed();
	}
	else{
		m_Velocity.y = 0;
	}

	m_Velocity.y += m_JumpVelocity;

	velocityVector = XMLoadFloat3(&m_Velocity);
	XMVECTOR displacementVector = velocityVector*gameContext.pGameTime->GetElapsed();
	XMFLOAT3 displacementFloat;
	XMStoreFloat3(&displacementFloat, displacementVector);

	this->GetComponent<ControllerComponent>()->GetTransform()->Rotate(0, m_TotalYaw, 0);
	this->GetComponent<ControllerComponent>()->Move(displacementFloat);

	//////////////////////////////////////////////////////////
	//build action pressed

	if (gameContext.pInput->IsActionTriggered(HeroCharacterMovement::BUILD)){
		animator->SetAnimation(L"Attack");
		BuildSelected();
	}

	if (gameContext.pInput->IsActionTriggered(HeroCharacterMovement::SELECTLEFTBUILDING) && !m_pInterface->IsButtonBusy() && m_CurrentBuildingSelected != 0){
		m_pInterface->MoveSelectorToLeft(gameContext);
		m_CurrentBuildingSelected--;
		cout << m_CurrentBuildingSelected;
	}

	if (gameContext.pInput->IsActionTriggered(HeroCharacterMovement::SELECTRIGHTBUILDING) && !m_pInterface->IsButtonBusy() && m_CurrentBuildingSelected < COUNTBUILDINGS-1){
		m_pInterface->MoveSelectorToRight(gameContext);
		m_CurrentBuildingSelected++;
		cout << m_CurrentBuildingSelected;
	}

}



void HeroCharacter::zoomCameraIn(const GameContext& gameContext)
{
	XMFLOAT3 camPos = m_pCamera->GetTransform()->GetPosition();

	PxVec3 camDirectionPx = -PxVec3(camPos.x, camPos.y, camPos.z);
	camDirectionPx.normalize();

	XMFLOAT3 camDirFloat3 = XMFLOAT3(camDirectionPx.x, camDirectionPx.y, camDirectionPx.z);
	XMVECTOR camDirection = XMLoadFloat3(&camDirFloat3);
	XMVECTOR camPosVec = XMLoadFloat3(&camPos);
	if (m_minCamDistance <  XMVectorGetX(XMVector3Length((XMLoadFloat3(&camPos)))))
	{
		m_pCamera->GetTransform()->Translate(camPosVec + camDirection * m_CamMoveSpeed * gameContext.pGameTime->GetElapsed());
	}
}

void HeroCharacter::zoomCameraOut(const GameContext& gameContext)
{	
	XMFLOAT3 camPos = m_pCamera->GetTransform()->GetPosition();

	PxVec3 camDirectionPx = -PxVec3(camPos.x, camPos.y, camPos.z);
	camDirectionPx.normalize();

	XMFLOAT3 camDirFloat3 = XMFLOAT3(camDirectionPx.x, camDirectionPx.y, camDirectionPx.z);
	XMVECTOR camDirection = XMLoadFloat3(&camDirFloat3);
	XMVECTOR camPosVec = XMLoadFloat3(&camPos);
	if (m_maxCamDistance >  XMVectorGetX(XMVector3Length((XMLoadFloat3(&camPos)))))
	{
		m_pCamera->GetTransform()->Translate(camPosVec + -camDirection * m_CamMoveSpeed * gameContext.pGameTime->GetElapsed());
	}
}

void HeroCharacter::SetSelectedBuilding(BuildingSelected currentBuilding){
	m_CurrentBuildingSelected = currentBuilding;
}

BuildingPlot* HeroCharacter::CheckForBuildSpot(){

	auto physxProxy = GetScene()->GetPhysxProxy();

	auto startPos = GetTransform()->GetWorldPosition();
	auto forward = GetTransform()->GetForward();
	float offsetDir = 1.f;

	PxVec3 rayStart = PxVec3(startPos.x, startPos.y, startPos.z) + PxVec3(forward.x, forward.y, forward.z)*2;

	PxVec3 rayDir = PxVec3(forward.x*offsetDir, -1.f, forward.z*offsetDir);
	rayDir.normalize();

	PxRaycastBuffer hit;

	if (physxProxy->Raycast(rayStart, rayDir, PX_MAX_F32, hit)){
		auto obj = dynamic_cast<BuildingPlot*>(((BaseComponent*)hit.block.actor->userData)->GetGameObject());
		if (obj){
			return obj;
		}
	}

	return nullptr;
}
void HeroCharacter::BuildSelected(){

	Building *selectedBuilding = nullptr;
	BuildingPlot* plot = CheckForBuildSpot();
	if (!plot)
		return;

	switch (m_CurrentBuildingSelected)
	{
	case BuildingSelected::HUMANBARRACKS:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanBarracksNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanBarracksNum);

			if (plot->IsBuildOn()){
				plot->Reset();
			}
			plot->buildOn();

			selectedBuilding = m_pBuildingFactory->CreateHumanBarracks();
			selectedBuilding->SetPlayerBuilding(true);
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0, 3, 0));
			selectedBuilding->GetTransform()->Scale(4.f, 4.f, 4.f);
			plot->AddChild(selectedBuilding);
		}
		break;
	case BuildingSelected::HUMANRANGE:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanRangeNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanRangeNum);

			if (plot->IsBuildOn()){
				plot->Reset();
			}
			plot->buildOn();

			selectedBuilding = m_pBuildingFactory->CreateHumanRange();
			selectedBuilding->SetPlayerBuilding(true);
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0, 3, 0));
			selectedBuilding->GetTransform()->Scale(0.05f, 0.05f, 0.05f);
			plot->AddChild(selectedBuilding);
		}
		break;
	case BuildingSelected::HUMANSTABLES:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanStablesNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanStablesNum);

			if (plot->IsBuildOn()){
				plot->Reset();
			}
			plot->buildOn();

			selectedBuilding = m_pBuildingFactory->CreateHumanStables();
			selectedBuilding->SetPlayerBuilding(true);
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0, 3, 0));
			selectedBuilding->GetTransform()->Scale(0.05f, 0.05f, 0.05f);
			plot->AddChild(selectedBuilding);
		}
		break;
	case BuildingSelected::HUMANSIEGESHOP:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanSiegeShopNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanSiegeShopNum);
			selectedBuilding = m_pBuildingFactory->CreateHumanSiegeShop();

			if (plot->IsBuildOn()){
				plot->Reset();
			}
			plot->buildOn();

			selectedBuilding->SetPlayerBuilding(true);
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0, 3, 0));
			selectedBuilding->GetTransform()->Scale(0.07f, 0.07f, 0.07f);
			plot->AddChild(selectedBuilding);
		}
		break;

	default:
		break;
	}
}

void HeroCharacter::Reset()
{

}