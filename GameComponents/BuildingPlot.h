#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;

class BuildingPlot : public GameObject
{
public:
	BuildingPlot(float width, float height, float depth);
	virtual ~BuildingPlot();

	virtual void Initialize(const GameContext& gameContext);
	
	bool IsBuildOn() const { return m_IsBuildOn; }
	void buildOn(bool buildOn = true) { m_IsBuildOn = buildOn; }
	void Reset();

private:

	XMFLOAT3 m_Dimensions;
	bool m_IsBuildOn;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	BuildingPlot(const BuildingPlot& t);
	BuildingPlot& operator=(const BuildingPlot& t);
};

