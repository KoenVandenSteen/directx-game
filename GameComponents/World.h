#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;
class GameWall;

class World : public GameObject
{
public:
	World();
	~World();

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);

	bool m_EntranceTriggered = false;
	void Reset();
private:
	GameWall *m_pEntranceMineL = nullptr, *m_pEntranceMineR = nullptr;


private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	World(const World& t);
	World& operator=(const World& t);
};

