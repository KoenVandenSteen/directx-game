//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "World.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../../Materials/UberMaterial.h"
#include "Content/ContentManager.h"
#include "GameWall.h"
#include "../TheGameScene.h"
#include "../../CourseObjects/Week 11/Cubemap.h"
#include "BuildingPlot.h"
World::World()
{
}


World::~World(void)
{
}

void World::Initialize(const GameContext& gameContext)
{
	//lEVEL CREATION
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto defaultMaterial = physX->createMaterial(1.f, 1.f, 0.f);
	//
	//
	auto levelMesh = new ModelComponent(L"./TheGame/Meshes/Level.ovm");
	//
	//
	auto myMaterial = new DiffuseMaterial();
	//
	auto LevelRigidbody = new RigidBodyComponent(true);
	//
	gameContext.pMaterialManager->AddMaterial(myMaterial, 60);
	myMaterial->SetDiffuseTexture(L"./TheGame/Textures/LevelTexture.dds");
	levelMesh->SetMaterial(60);
	
	AddComponent(levelMesh);
	
	AddComponent(LevelRigidbody);
	std::shared_ptr<PxGeometry> levelGeometry(new PxTriangleMeshGeometry(ContentManager::Load<PxTriangleMesh>(L"./TheGame/Meshes/Level.ovpt")));
	AddComponent(new ColliderComponent(levelGeometry, *defaultMaterial, PxTransform::createIdentity()));



	//TRIGGER BOXES

	m_pEntranceMineL = new GameWall(20, 20, 10, true);
	this->AddChild(m_pEntranceMineL);
	m_pEntranceMineL->GetTransform()->Translate(-200, 10, -300);

	m_pEntranceMineR = new GameWall(20, 20, 10, true);
	this->AddChild(m_pEntranceMineR);
	m_pEntranceMineR->GetTransform()->Translate(200, 10, -300);

	//cave creation

	auto cave = new GameObject();

	auto caveMesh = new ModelComponent(L"./TheGame/Meshes/Cave.ovm");

	auto caveMaterial = new DiffuseMaterial();

	gameContext.pMaterialManager->AddMaterial(caveMaterial, 50);
	caveMaterial->SetDiffuseTexture(L"./TheGame/Textures/CaveTexture.dds");
	//caveMaterial->EnableNormalMapping("true");
	caveMesh->SetMaterial(50);
	
	cave->AddComponent(caveMesh);
	//caveMesh->GetTransform()->Rotate(0, 90, 0);
	//cave->GetTransform()->Rotate(0, 90, 0);
	auto caveRigidbody = new RigidBodyComponent(true);
	
	cave->AddComponent(caveRigidbody);
	
	//cave->GetTransform()->Rotate(0, 90, 0);
	std::shared_ptr<PxGeometry> caveGeometry(new PxTriangleMeshGeometry(ContentManager::Load<PxTriangleMesh>(L"./TheGame/Meshes/Cave.ovpt"),PxMeshScale(0.5)));
	cave->AddComponent(new ColliderComponent(caveGeometry, *defaultMaterial, PxTransform::createIdentity()));
	
	AddChild(cave);
	cave->GetTransform()->Scale(0.5, 0.5, 0.5);
	cave->GetTransform()->Translate(500,50, 0);
	
	auto skybox = new Cubemap(L"./Resources/Textures/SkyBox.dds");
	AddChild(skybox);


	//building boxes
	UINT BuildingSlots = 9;
	float BuildingSlotSize = 20.f;
	float BuildSlotOffsetSize = 5.f;
	XMFLOAT3 startPos = XMFLOAT3(-105, 0, -280);
	
	auto material = new DiffuseMaterial();
	material->SetDiffuseTexture(L"./TheGame/Textures/DefaultTexture.dds");
	gameContext.pMaterialManager->AddMaterial(material, 20);

	for (UINT i = 0; i < BuildingSlots; ++i)
	{
		BuildingPlot *buildingPlot = new BuildingPlot(BuildingSlotSize, 5.f, BuildingSlotSize);
		buildingPlot->GetTransform()->Translate(startPos.x + (BuildingSlotSize + BuildSlotOffsetSize)*i, startPos.y, startPos.z);
		AddChild(buildingPlot);
	}

	
	//auto LevelEdge = new GameWall(250, 20, 10);
	//this->AddChild(LevelEdge);
	//LevelEdge->GetTransform()->Translate(0, 10, -282);

}


void World::Update(const GameContext& gameContext){
	
	UNREFERENCED_PARAMETER(gameContext);
	if (m_pEntranceMineL->IsTriggered() || m_pEntranceMineR->IsTriggered()){

		m_pEntranceMineL->Reset();
		m_pEntranceMineR->Reset();

		if (static_cast<TheGameScene*>(GetScene())->GetCurrentHeroState() == TheGameScene::HeroState::BuildStage){
			m_EntranceTriggered = true;
		}
	}
	
}

void World::Reset()
{
	auto buildPlots = GetChildren<BuildingPlot>();
	for (auto buildPlot : buildPlots)
	{
		buildPlot->Reset();
	}
}