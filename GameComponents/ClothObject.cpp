#include "Base\stdafx.h"

#include "ClothObject.h"
#include "Components\Components.h"
#include "Content\ContentManager.h"
#include "Scenegraph\GameObject.h"
#include "Scenegraph\GameScene.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
#include "../../../OverlordEngine/Helpers/PhysxHelper.h"
ClothObject::ClothObject(XMFLOAT4 color, PxVec3 StartPos) :
m_meshDrawer(nullptr),
m_color(color),
m_StartPos(StartPos)
{
}


ClothObject::~ClothObject()
{
}

void ClothObject::Initialize(const GameContext& gameContext){

	UNREFERENCED_PARAMETER(gameContext);

	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto physxProxy = this->GetScene()->GetPhysxProxy();
	// create regular mesh
	PxU32 resolution = 20;
	PxU32 numParticles = resolution*resolution;
	PxU32 numTriangles = 2 * (resolution - 1)*(resolution - 1);

	// create cloth particles
	PxClothParticle *particles = new PxClothParticle[numParticles];
	PxVec3 center(0.0f, 0.0f, 0.0f);
	PxVec3 delta = 1.0f / (resolution - 1) * PxVec3(15.0f, 15.0f, 15.0f);
	PxClothParticle* pIt = particles;
	for (PxU32 i = 0; i<resolution; ++i)
	{
		for (PxU32 j = 0; j<resolution; ++j, ++pIt)
		{
			pIt->invWeight = j + 1<resolution ? 1.0f : 0.0f;
			pIt->pos = m_StartPos + delta.multiply(PxVec3(PxReal(i),
				PxReal(j), -PxReal(j))) - center;
		}
	}

	
	// create triangles
	PxU32 *triangles = new PxU32[3 * numTriangles];
	m_indices.reserve(3 * numTriangles);
	PxU32* iIt = triangles;
	for (PxU32 i = 0; i<resolution - 1; ++i)
	{
		for (PxU32 j = 0; j<resolution - 1; ++j)
		{
			PxU32 odd = j & 1u, even = 1 - odd;
			//traingle1
			*iIt++ = i*resolution + (j + odd);
			m_indices.push_back(i*resolution + (j + odd));
			*iIt++ = (i + odd)*resolution + (j + 1);
			m_indices.push_back((i + odd)*resolution + (j + 1));
			*iIt++ = (i + 1)*resolution + (j + even);
			m_indices.push_back((i + 1)*resolution + (j + even));

			//traignle2
			*iIt++ = (i + 1)*resolution + (j + even);
			m_indices.push_back((i + 1)*resolution + (j + even));
			*iIt++ = (i + even)*resolution + j;
			m_indices.push_back((i + even)*resolution + j);
			*iIt++ = i*resolution + (j + odd);
			m_indices.push_back(i*resolution + (j + odd));

		}
	}

	m_meshDrawer = new MeshDrawComponent(numTriangles);
	
	for (size_t i = 0; i < numTriangles*3; i+=3)
	{
		VertexPosNormCol vertex1, vertex2, vertex3;
		vertex1.Position = ToXMFLOAT3(particles[triangles[i]].pos);
		vertex1.Color = m_color;
		vertex1.Normal = XMFLOAT3(1, 0, 0);
		
		vertex2.Position = ToXMFLOAT3(particles[triangles[i+1]].pos);
		vertex2.Color = m_color;
		vertex2.Normal = XMFLOAT3(1, 0, 0);
		vertex3.Position = ToXMFLOAT3(particles[triangles[i+2]].pos);
		vertex3.Color = m_color;
		vertex3.Normal = XMFLOAT3(1, 0, 0);

		m_meshDrawer->AddTriangle(vertex1, vertex2, vertex3);
	}


	AddComponent(m_meshDrawer);

	// create fabric from mesh
	PxClothMeshDesc meshDesc;
	meshDesc.points.count = numParticles;
	meshDesc.points.stride = sizeof(PxClothParticle);
	meshDesc.points.data = particles;

	meshDesc.invMasses.count = numParticles;
	meshDesc.invMasses.stride = sizeof(PxClothParticle);
	meshDesc.invMasses.data = &particles->invWeight;

	meshDesc.triangles.count = numTriangles;
	meshDesc.triangles.stride = 3 * sizeof(PxU32);
	meshDesc.triangles.data = triangles;


	// cook fabric
	PxClothFabric* fabric = PxClothFabricCreate(*physX, meshDesc, PxVec3(0, 1, 0));

	delete[] triangles;

	// create cloth
	PxTransform gPose = PxTransform(PxVec3(0, 1, 0));
	m_Cloth = physX->createCloth(gPose, *fabric, particles, PxClothFlags(0));

	fabric->release();
	delete[] particles;

	// 240 iterations per/second (4 per-60hz frame)
	m_Cloth->setSolverFrequency(240.0f);

	physxProxy->GetPhysxScene()->addActor(*m_Cloth);
	

}

void ClothObject::Update(const GameContext& gameContext){

	UNREFERENCED_PARAMETER(gameContext);
	PxU32 resolution = 20;
	PxU32 numTriangles = 2 * (resolution - 1)*(resolution - 1);

	m_meshDrawer->RemoveTriangles();

	auto data = m_Cloth->lockParticleData();
	for (size_t i = 0; i < numTriangles * 3; i += 3)
	{
		VertexPosNormCol vertex1, vertex2, vertex3;
		vertex1.Position = ToXMFLOAT3(data->particles[m_indices[i]].pos);
		vertex1.Color = m_color;
		vertex1.Normal = XMFLOAT3(1, 0, 0);

		vertex2.Position = ToXMFLOAT3(data->particles[m_indices[i + 1]].pos);
		vertex2.Color = m_color;
		vertex2.Normal = XMFLOAT3(1, 0, 0);
		vertex3.Position = ToXMFLOAT3(data->particles[m_indices[i + 2]].pos);
		vertex3.Color = m_color;
		vertex3.Normal = XMFLOAT3(1, 0, 0);

		m_meshDrawer->AddTriangle(vertex1, vertex2, vertex3);
	}

	m_meshDrawer->UpdateBuffer();

}