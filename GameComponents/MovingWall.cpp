//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "MovingWall.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../../Materials/UberMaterial.h"
#include "Content/ContentManager.h"
#include "GameWall.h"
#include "../TheGameScene.h"
#include "HeroCharacter.h"
MovingWall::MovingWall(UINT matIndex) :
m_walSpeed(50.0f),
m_matIndex(matIndex)
{
}


MovingWall::~MovingWall(void)
{
}

void MovingWall::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//lEVEL CREATION
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto defaultMaterial = physX->createMaterial(0.5, 0.5, 1);
	
	auto levelMesh = new ModelComponent(L"./TheGame/Meshes/MovingWall.ovm");


	levelMesh->SetMaterial(m_matIndex);

	AddComponent(levelMesh);

	auto rigidbody = new RigidBodyComponent(false);
	rigidbody->SetKinematic(true);
	AddComponent(rigidbody);
	std::shared_ptr<PxGeometry> boxGeometry(new PxConvexMeshGeometry(ContentManager::Load<PxConvexMesh>(L"./TheGame/Meshes/MovingWall.ovpc")));

	auto colider = new ColliderComponent(boxGeometry, *defaultMaterial, PxTransform::createIdentity());
	AddComponent(colider);
	colider->EnableTrigger(true);

	this->SetOnTriggerCallBack([this](GameObject* trigger, GameObject* receiver, TriggerAction action)
	{
		UNREFERENCED_PARAMETER(receiver);
		UNREFERENCED_PARAMETER(trigger);
		if (action == TriggerAction::ENTER){
			if (dynamic_cast<HeroCharacter*>(receiver) != nullptr)
				static_cast<TheGameScene*>(GetScene())->SetCurrentHeroState(TheGameScene::HeroState::BuildStage);
		}
	});
}


void MovingWall::Update(const GameContext& gamecontext){

	XMVECTOR move = { 0, 0, 1 };
	GetTransform()->Translate(XMLoadFloat3(&GetTransform()->GetPosition()) - move*gamecontext.pGameTime->GetElapsed() * m_walSpeed);

}