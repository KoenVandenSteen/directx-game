#pragma once
#include "Scenegraph/GameObject.h"

class ParticleEffect;

class ParticleManager : public GameObject
{
public:
	ParticleManager();
	virtual ~ParticleManager();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);

	void SpawnParticleEffect(XMFLOAT3 position, float duration, string name);
	void AddParticleEffect(const string name, const XMFLOAT3 velocity, const  float minSize, const  float maxSize, const  float minEnergy, const  float maxEnergy, const  float minSizeGrow, const  float maxSizeGrow, const  float minEmitterRange, const float maxEmmitterRange, const  XMFLOAT4 color, const  wstring &assetFile);
	static ParticleManager* GetParticleManager(){ return m_pParticleManager ? m_pParticleManager : m_pParticleManager = new ParticleManager(); };

private:


	map<string, ParticleEffect*> m_ParticleEffects;
	vector<ParticleEffect*> m_currParticleEffects;
	static ParticleManager* m_pParticleManager;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	ParticleManager(const ParticleManager& t);
	ParticleManager& operator=(const ParticleManager& t);
};

