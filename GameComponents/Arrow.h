#pragma once
#include "Scenegraph/GameObject.h"


class Arrow : public GameObject
{
public:
	Arrow();
	virtual ~Arrow();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);

	float GetLifeTime(){ return m_lifeTime; };

private:

	bool m_IsBuildOn;
	float m_lifeTime = 0;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Arrow(const Arrow& t);
	Arrow& operator=(const Arrow& t);
};

