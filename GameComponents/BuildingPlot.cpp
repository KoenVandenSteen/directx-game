//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "BuildingPlot.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "HeroCharacter.h"
#include "../../Materials/DiffuseMaterial.h"

#include "..\Buildings\HumanBarracks.h"
#include "..\Buildings\HumanRange.h"
#include "..\Buildings\HumanSiegeShop.h"
#include "..\Buildings\HumanStables.h"

BuildingPlot::BuildingPlot(float width, float height, float depth):
	m_Dimensions(width,height,depth),
	m_IsBuildOn(false)
{
}


BuildingPlot::~BuildingPlot(void)
{
}

void BuildingPlot::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	
	
	auto defaultMaterial = physX->createMaterial(0, 0, 1);

	auto buildingPlotMesh = new ModelComponent(L"./TheGame/Meshes/BuildingPlot.ovm");
	AddComponent(buildingPlotMesh);

	buildingPlotMesh->SetMaterial(20);

	auto rigidbody = new RigidBodyComponent(false);
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(m_Dimensions.x/2, m_Dimensions.y/2, m_Dimensions.z/2));
	auto collider = new ColliderComponent(geom, *defaultMaterial);
	
	AddComponent(rigidbody);
	rigidbody->SetKinematic(true);
	AddComponent(collider);
}

void BuildingPlot::Reset(){

		if (GetChild<HumanBarracks>())
			RemoveChild(GetChild<HumanBarracks>());
		if (GetChild<HumanRange>())
			RemoveChild(GetChild<HumanRange>());
		if (GetChild<HumanStables>())
			RemoveChild(GetChild<HumanStables>());
		if (GetChild<HumanSiegeShop>())
			RemoveChild(GetChild<HumanSiegeShop>());

}