#pragma once
#include "Scenegraph/GameObject.h"

class BuildingPlot;
class BuildingFactory;

enum BuildingTypes
{
	Barracks,
	Range,
	Stable,
	Siege,
	countBuildings
};

inline BuildingTypes operator++(BuildingTypes &curBuilding, int)
{
	if (curBuilding != countBuildings - 1)
		curBuilding = static_cast<BuildingTypes>(curBuilding + 1);
	return curBuilding;
}

inline BuildingTypes operator--(BuildingTypes &curBuilding, int)
{
	if (curBuilding != 0)
		curBuilding = static_cast<BuildingTypes>(curBuilding - 1);
	return curBuilding;
}


class EnemyPlayer : public GameObject
{
public:

	EnemyPlayer();

	virtual ~EnemyPlayer();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);

	void Reset();

private:


	int m_currentBuildSpot = 0;
	vector<BuildingPlot*> m_BuildingPlots;
	BuildingFactory *m_pBuildingFactory = nullptr;
	void BuildSelected(BuildingTypes building);
	float m_currentAmmountOfGold = 50;
	float m_startGold = 50;
	float m_goldCounter = 7.5;
	BuildingPlot *GetNewBuildPlot();
	BuildingTypes m_currentBuildLevel;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	EnemyPlayer(const EnemyPlayer& t);
	EnemyPlayer& operator=(const EnemyPlayer& t);

};


