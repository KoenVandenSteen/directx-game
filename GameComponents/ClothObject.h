#pragma once
#include "Scenegraph/GameObject.h"

class MeshDrawComponent;

class ClothObject : public GameObject
{
public:
	ClothObject(XMFLOAT4 color, PxVec3 StartPos);
	~ClothObject(void);


	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
private:

	MeshDrawComponent *m_meshDrawer;
	PxCloth *m_Cloth = nullptr;
	vector<PxU32> m_indices;

	XMFLOAT4 m_color;
	PxVec3 m_StartPos;
private:
	// -------------------------
	// Disabling default copy constructor and default
	// assignment operator.
	// -------------------------
	ClothObject(const ClothObject& yRef);
	ClothObject& operator=(const ClothObject& yRef);

};
