//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "Interface.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../../Materials/UberMaterial.h"
#include "Content/ContentManager.h"
#include "GameWall.h"
#include "../TheGameScene.h"

#include "Graphics\TextRenderer.h"
#include "../../../OverlordEngine/Components/SpriteComponent.h"
Interface::Interface() :
	m_buttonSpeed(120),
	m_buttonOffset(82)
{
}


Interface::~Interface(void)
{
}

void Interface::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pSpriteFont = ContentManager::Load<SpriteFont>(L"./Resources/SpriteFonts/Consolas_32.fnt");

	//TEST1
	auto UI = new GameObject();
	UI->AddComponent(new SpriteComponent(L"./TheGame/Textures/InterfaceTexture.dds", XMFLOAT2(0, 0), XMFLOAT4(1, 1, 1, 1.0f)));

	AddChild(UI);

	m_selectButton = new GameObject();
	m_selectButton->AddComponent(new SpriteComponent(L"./TheGame/Textures/SelectButton.dds", XMFLOAT2(0, 0), XMFLOAT4(1, 1, 1, 1.0f)));

	AddChild(m_selectButton);

	m_selectButton->GetTransform()->Translate(465,638,0);

	m_startPos = m_selectButton->GetTransform()->GetPosition();
	m_endPos = XMFLOAT3(m_selectButton->GetTransform()->GetPosition().x + m_buttonOffset, m_selectButton->GetTransform()->GetPosition().y, m_selectButton->GetTransform()->GetPosition().z);

	m_pMenu = new GameObject();
	m_pMenu->AddComponent(new SpriteComponent(L"./TheGame/Textures/MenuTexture.dds", XMFLOAT2(0, 0), XMFLOAT4(1, 1, 1, 1.0f)));
	m_pMenu->GetTransform()->Translate(390,75,0);
	AddChild(m_pMenu);

	m_pGameScene = static_cast<TheGameScene*>(GetScene());
}


void Interface::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
void Interface::Update(const GameContext& gamecontext){
	UNREFERENCED_PARAMETER(gamecontext);
}

void Interface::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto textRenderer = TextRenderer::GetInstance();
	wstring interfaceText;

	interfaceText = L"Gold: " + to_wstring(m_gold);
	textRenderer->DrawText(m_pSpriteFont, interfaceText, XMFLOAT2(10, 10), (XMFLOAT4)Colors::White);
	interfaceText = L"Base Hp: " + to_wstring(m_baseHitPoints);
	textRenderer->DrawText(m_pSpriteFont, interfaceText, XMFLOAT2(10, 35), (XMFLOAT4)Colors::White);
	interfaceText = L"Enemy Base Hp: " + to_wstring(m_enemyBaseHitPoints);
	textRenderer->DrawText(m_pSpriteFont, interfaceText, XMFLOAT2(10, 60), (XMFLOAT4)Colors::White);

	
	wstring tempText;
	switch (m_pGameScene->GetCurrentGameState())
	{
	case TheGameScene::GameState::Start:	
		tempText = L"Welcome to THE Game";
		textRenderer->DrawText(m_pSpriteFont, tempText, XMFLOAT2(480, 120), (XMFLOAT4)Colors::White);

		tempText = L"Collect, build, conquer!";
		textRenderer->DrawText(m_pSpriteFont, tempText, XMFLOAT2(440, 300), (XMFLOAT4)Colors::White);

		tempText = L"Press To Start";
		textRenderer->DrawText(m_pSpriteFont, tempText, XMFLOAT2(500, 500), (XMFLOAT4)Colors::White);

		m_pMenu->GetTransform()->Scale(1,1,1);
		break;

	case TheGameScene::GameState::Play:
		tempText = L"";
		textRenderer->DrawText(m_pSpriteFont, tempText, XMFLOAT2(480, 120), (XMFLOAT4)Colors::White);
		m_pMenu->GetTransform()->Scale(0, 0, 0);
		break;
	case TheGameScene::GameState::End:
		if (m_baseHitPoints>0)
			tempText = L"Vicotry!!!";
		else
			tempText = L"You loose!!!";

		textRenderer->DrawText(m_pSpriteFont, tempText, XMFLOAT2(480, 120), (XMFLOAT4)Colors::White);

		tempText = L"Play Again?!";
		textRenderer->DrawText(m_pSpriteFont, tempText, XMFLOAT2(440, 300), (XMFLOAT4)Colors::White);

		tempText = L"Press To Start";
		textRenderer->DrawText(m_pSpriteFont, tempText, XMFLOAT2(500, 500), (XMFLOAT4)Colors::White);
		m_pMenu->GetTransform()->Scale(1, 1, 1);
		break;
	default:
		break;
	}

	if (m_selectButton->GetTransform()->GetPosition().x > m_endPos.x && m_MovingLeft)
	{
		m_selectButton->GetTransform()->Translate(m_selectButton->GetTransform()->GetPosition().x - gameContext.pGameTime->GetElapsed() * m_buttonSpeed, m_startPos.y, m_startPos.z);
	}
	else{
		m_MovingLeft = false;
	}

	if (m_selectButton->GetTransform()->GetPosition().x < m_endPos.x && m_MovingRight)
	{
		m_selectButton->GetTransform()->Translate(m_selectButton->GetTransform()->GetPosition().x + gameContext.pGameTime->GetElapsed() * m_buttonSpeed, m_startPos.y, m_startPos.z);
	}
	else{
		m_MovingRight = false;
	}



}

void Interface::MoveSelectorToLeft(const GameContext& gameContext)
{

	UNREFERENCED_PARAMETER(gameContext);
	m_MovingLeft = true;
	m_startPos = m_selectButton->GetTransform()->GetPosition();
	m_endPos = XMFLOAT3(m_startPos.x - m_buttonOffset, m_startPos.y, m_startPos.z);
}

void Interface::MoveSelectorToRight(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_MovingRight = true;
	m_startPos = m_selectButton->GetTransform()->GetPosition();
	m_endPos = XMFLOAT3(m_startPos.x + m_buttonOffset, m_startPos.y, m_startPos.z);
}