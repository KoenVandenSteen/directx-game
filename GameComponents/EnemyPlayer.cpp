//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "EnemyPlayer.h"
#include "Components\Components.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameScene.h"

#include "../Units/UnitManager.h"
#include "BuildingPlot.h"
#include "../Buildings/BuildingFactory.h"

#include "..\Buildings\HumanBarracks.h"
#include "..\Buildings\HumanRange.h"
#include "..\Buildings\HumanSiegeShop.h"
#include "..\Buildings\HumanStables.h"

EnemyPlayer::EnemyPlayer()
{
}

EnemyPlayer::~EnemyPlayer(void)
{

}

void EnemyPlayer::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//building boxes
	UINT BuildingSlots = 9;
	float BuildingSlotSize = 20.f;
	float BuildSlotOffsetSize = 5.f;
	XMFLOAT3 startPos = XMFLOAT3(-105, 0, 850);

	for (UINT i = 0; i < BuildingSlots; ++i)
	{
		BuildingPlot *buildingPlot = new BuildingPlot(BuildingSlotSize, 5.f, BuildingSlotSize);
		buildingPlot->GetTransform()->Translate(startPos.x + (BuildingSlotSize + BuildSlotOffsetSize)*i, startPos.y, startPos.z);
		AddChild(buildingPlot);
		m_BuildingPlots.push_back(buildingPlot);
	}
	
	m_pBuildingFactory = BuildingFactory::GetInstance();
	m_currentBuildLevel = BuildingTypes::Barracks;
}

void EnemyPlayer::Update(const GameContext& gameContext){

	UNREFERENCED_PARAMETER(gameContext);
	



	if (GetNewBuildPlot())
		BuildSelected(m_currentBuildLevel);

	if (m_currentBuildSpot > 8){
		m_currentBuildLevel++;
		m_currentBuildSpot = 0;
	}

	m_currentAmmountOfGold += gameContext.pGameTime->GetElapsed() * m_goldCounter;

}


void EnemyPlayer::BuildSelected(BuildingTypes type){

	Building *selectedBuilding = nullptr;

	BuildingPlot *newPlot = nullptr;

	switch (type)
	{
	case BuildingTypes::Barracks:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanBarracksNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanBarracksNum);
			newPlot = GetNewBuildPlot();
			selectedBuilding = m_pBuildingFactory->CreateHumanBarracks();
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0,3,0));
			selectedBuilding->GetTransform()->Scale(4.f, 4.f, 4.f);
			selectedBuilding->SetPlayerBuilding(false);
			if (newPlot){
				newPlot->AddChild(selectedBuilding);
				newPlot->buildOn();
			}
			m_currentBuildSpot++;
		}
		break;
	case BuildingTypes::Range:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanRangeNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanRangeNum);
			newPlot = GetNewBuildPlot();
			selectedBuilding = m_pBuildingFactory->CreateHumanRange();
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0,3, 0));
			selectedBuilding->GetTransform()->Scale(0.05f, 0.05f, 0.05f);
			selectedBuilding->SetPlayerBuilding(false);
			if (newPlot){
				newPlot->AddChild(selectedBuilding);
				newPlot->buildOn();
			}
			m_currentBuildSpot++;
		}
		break;
	case BuildingTypes::Siege:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanStablesNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanStablesNum);
			newPlot = GetNewBuildPlot();
			selectedBuilding = m_pBuildingFactory->CreateHumanStables();
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0, 3, 0));
			selectedBuilding->GetTransform()->Scale(0.05f, 0.05f, 0.05f);
			selectedBuilding->SetPlayerBuilding(false);
			if (newPlot){
				newPlot->AddChild(selectedBuilding);
				newPlot->buildOn();
			}
			m_currentBuildSpot++;
		}
		break;
	case BuildingTypes::Stable:
		if (m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanSiegeShopNum) <= m_currentAmmountOfGold){
			m_currentAmmountOfGold -= m_pBuildingFactory->ReturnBuildingCost(BuildingFactory::BuildingList::HumanSiegeShopNum);
			newPlot = GetNewBuildPlot();
			selectedBuilding = m_pBuildingFactory->CreateHumanSiegeShop();
			selectedBuilding->GetTransform()->Translate(XMFLOAT3(0,3, 0));
			selectedBuilding->GetTransform()->Scale(0.07f, 0.07f, 0.07f);
			selectedBuilding->SetPlayerBuilding(false);
			if (newPlot){
				newPlot->AddChild(selectedBuilding);
				newPlot->buildOn();
			}
			m_currentBuildSpot++;
		}
		break;

	default:
		break;
	}
}

BuildingPlot *EnemyPlayer::GetNewBuildPlot()
{
	BuildingPlot *nextPlot = nullptr;

	for (auto plot : m_BuildingPlots)
	{
		if (!plot->IsBuildOn())
		{
			return plot;
		}
		else
		{
			m_BuildingPlots.at(m_currentBuildSpot)->Reset();
			return m_BuildingPlots.at(m_currentBuildSpot);
		}
	}
	
	return nextPlot;
}

void EnemyPlayer::Reset()
{
	auto buildPlots  = GetChildren<BuildingPlot>();

	for (auto buildPlot : buildPlots)
	{
		buildPlot->Reset();
	}

	m_currentAmmountOfGold = m_startGold;
	m_currentBuildLevel = BuildingTypes::Barracks;
	m_currentBuildSpot = 0;
}