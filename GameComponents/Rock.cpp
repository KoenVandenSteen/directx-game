//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "Rock.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"


Rock::Rock()
{

}


Rock::~Rock(void)
{

}

void Rock::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto defaultMaterial = physX->createMaterial(0,0,0);
	

	auto RockMesh = new ModelComponent(L"./TheGame/Meshes/Rock.ovm");
	AddComponent(RockMesh);

	RockMesh->SetMaterial(20);
	
	auto rigidbody = new RigidBodyComponent();
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(0.1f, 0.1f, 0.1f));
	auto collider = new ColliderComponent(geom, *defaultMaterial);

	AddComponent(collider);
	AddComponent(rigidbody);

	GetTransform()->Scale(0.07f, 0.07f, 0.07f);
}

void Rock::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
