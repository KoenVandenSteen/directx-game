#pragma once
#include "Scenegraph/GameObject.h"

class ControllerComponent;
class CameraComponent;
class BuildingFactory;
class ModelComponent;
class Interface;
class BuildingPlot;

enum HeroCharacterMovement : UINT
{
	LEFT = 0,
	RIGHT,
	FORWARD,
	BACKWARD,
	BUILD,
	SELECTLEFTBUILDING,
	SELECTRIGHTBUILDING
};


enum BuildingSelected : UINT
{
	HUMANBARRACKS = 0,
	HUMANRANGE,
	HUMANSTABLES,
	HUMANSIEGESHOP,
	COUNTBUILDINGS
};


inline BuildingSelected operator++(BuildingSelected &curBuilding, int)
{
	if (curBuilding != COUNTBUILDINGS - 1)
		curBuilding = static_cast<BuildingSelected>(curBuilding + 1);
	return curBuilding;
}

inline BuildingSelected operator--(BuildingSelected &curBuilding, int)
{
	if (curBuilding != 0)
		curBuilding = static_cast<BuildingSelected>(curBuilding - 1);
	return curBuilding;
}

class HeroCharacter : public GameObject
{
public:



	HeroCharacter(float radius = 2, float height = 5, float moveSpeed = 100);
	virtual ~HeroCharacter();
	virtual void Initialize(const GameContext& gameContext);
	virtual void PostInitialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	void SetSelectedBuilding(BuildingSelected currentBuilding);
	void SetSpeed(float speed){
		m_MaxRunVelocity = speed;
	};
	void SetGold(float gold){ m_currentAmmountOfGold = gold; }
	float GetGold(){ return m_currentAmmountOfGold; }
	void CameraLookAtHero();
	void zoomCameraIn(const GameContext& gameContext);
	void zoomCameraOut(const GameContext& gameContext);
	void Reset();
protected:


	
	
private:
	float m_minCamDistance, m_maxCamDistance, m_CamMoveSpeed;

	BuildingPlot* CheckForBuildSpot();
	Interface *m_pInterface = nullptr;
	CameraComponent* m_pCamera;
	ControllerComponent* m_pController;
	ModelComponent* m_pModel = nullptr;
	float m_TotalPitch, m_TotalYaw;
	float m_MoveSpeed, m_RotationSpeed;
	float m_Radius, m_Height;
	float m_StartMoney;
	//Running
	float m_MaxRunVelocity,
		m_TerminalVelocity,
		m_Gravity,
		m_RunAccelerationTime,
		m_JumpAccelerationTime,
		m_RunAcceleration,
		m_JumpAcceleration,
		m_RunVelocity,
		m_JumpVelocity;
	XMFLOAT3 m_Velocity;
	void BuildSelected();
	BuildingSelected m_CurrentBuildingSelected = BuildingSelected::HUMANBARRACKS;
	BuildingFactory *m_pBuildingFactory = nullptr;
	float m_currentAmmountOfGold = 0;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	HeroCharacter(const HeroCharacter& t);
	HeroCharacter& operator=(const HeroCharacter& t);
};


