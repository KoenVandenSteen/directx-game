#pragma once
#include "Scenegraph/GameObject.h"


class Rock : public GameObject
{
public:
	Rock();
	virtual ~Rock();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);

	float GetLifeTime(){ return m_lifeTime; };

private:

	bool m_IsBuildOn;
	float m_lifeTime = 0;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Rock(const Rock& t);
	Rock& operator=(const Rock& t);
};

