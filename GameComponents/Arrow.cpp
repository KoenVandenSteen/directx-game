//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "Arrow.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"


Arrow::Arrow()
{

}


Arrow::~Arrow(void)
{

}

void Arrow::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto defaultMaterial = physX->createMaterial(0,0,0);
	

	auto ArrowMesh = new ModelComponent(L"./TheGame/Meshes/Arrow.ovm");
	AddComponent(ArrowMesh);

	ArrowMesh->SetMaterial(20);
	
	auto rigidbody = new RigidBodyComponent();
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(0.1f, 0.1f, 0.1f));
	auto collider = new ColliderComponent(geom, *defaultMaterial);

	AddComponent(collider);
	AddComponent(rigidbody);

	GetTransform()->Scale(0.07f, 0.07f, 0.07f);
}

void Arrow::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
