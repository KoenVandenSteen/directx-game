//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "Archer.h"
#include "Components\Components.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameScene.h"
#include "Physx/PhysxManager.h"
#include "Physx/PhysxProxy.h"
#include "..\..\Materials\SkinnedDiffuseMaterial.h"
#include "..\..\Materials\DiffuseMaterial.h"
#include "Graphics\ModelAnimator.h"
#include "UnitManager.h"
Archer::Archer(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit) :
Unit(enemyBase,speed,rotSpeed,range,playerUnit)
{
	m_Health = 50.f;
	m_Damage = 50.f;
}

Archer::~Archer(void)
{

}

void Archer::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Create controller
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto mat = physX->createMaterial(0, 0, 1);
	auto controller = new ControllerComponent(mat, m_Radius, m_Height, L"Archer", PxCapsuleClimbingMode::eEASY);

	AddComponent(controller);

	

	m_pModel = new ModelComponent(L"./TheGame/Meshes/RangedMinion.ovm");
	m_pModel->SetMaterial(UnitManager::UnitMaterial::ArcherMat);
	auto obj = new GameObject();
	obj->AddComponent(m_pModel);

	AddChild(obj);
	obj->GetTransform()->Scale(0.05f, 0.05f, 0.05f);
	obj->GetTransform()->Translate(0.f, -5.f, 0.f);
	obj->GetTransform()->Rotate(0.f, 180.f, 0.f);
	
	m_TargetPos = m_EnemyBase;

	GetTransform()->Translate(0,-500,0);
}








