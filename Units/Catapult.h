#pragma once
#include "Unit.h"




class Catapult : public Unit
{
public:

	Catapult(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit);

	virtual ~Catapult();

	void Initialize(const GameContext& gameContext);


protected:


private:


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Catapult(const Catapult& t);
	Catapult& operator=(const Catapult& t);
};


