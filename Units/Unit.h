#pragma once
#include "Scenegraph/GameObject.h"

class ModelComponent;
class Interface;
class ControllerComponent;


class Unit : public GameObject
{
public:

	Unit(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit);

	virtual ~Unit();

	virtual void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);

	bool IsPlayerUnit(){ return m_playerUnit; }
	XMVECTOR GetPosition();

	float GetCurrentHealth(){ return m_Health; };
	void ReceiveDamage(float damage){ m_Health -= damage; };

	void SetTargetBase(XMFLOAT3 target){ m_EnemyBase = target; };
	float GetDamage(){ return m_Damage; };

protected:
	ModelComponent* m_pModel = nullptr;
	float m_Radius, m_Height;
	XMFLOAT3 m_TargetPos;
	XMFLOAT3 m_EnemyBase;
	float m_Health;
	float m_Damage;
private:

	ControllerComponent* m_pController;

	void ShootArrow();
	float m_MoveSpeed, m_RotationSpeed;
	void ShootRock();

	float m_Range;
	
	Unit *m_CurrentTarget = nullptr;



	XMFLOAT3 ScanForEnemy();
	void Attack();
	bool m_playerUnit;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Unit(const Unit& t);
	Unit& operator=(const Unit& t);
};


