//Precompiled Header [ALWAYS ON TOP IN CPP]


#include "Base\stdafx.h"

#include "UnitManager.h"
#include "Components\Components.h"

#include "Unit.h"
#include "Archer.h"
#include "Catapult.h"
#include "Horse.h"
#include "Soldier.h"

#include "../../Materials/SkinnedDiffuseMaterial.h"
#include "../TheGameScene.h"
#include "../GameComponents/ParticleManager.h"

UnitManager *UnitManager::m_UnitManager;

UnitManager::UnitManager() 
{

}

UnitManager::~UnitManager(void)
{
	m_FriendlyMinions.clear();
	m_EnemyMinions.clear();
}

void UnitManager::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	auto soldierMat = new SkinnedDiffuseMaterial();
	soldierMat->SetDiffuseTexture(L"./TheGame/Textures/Soldier.dds");
	gameContext.pMaterialManager->AddMaterial(soldierMat, UnitMaterial::SoldierMat);

	auto horseMat = new SkinnedDiffuseMaterial();
	horseMat->SetDiffuseTexture(L"./TheGame/Textures/Horse.dds");
	gameContext.pMaterialManager->AddMaterial(horseMat, UnitMaterial::HorseMat);

	auto siegMat = new SkinnedDiffuseMaterial();
	siegMat->SetDiffuseTexture(L"./TheGame/Textures/cataPultTexture.dds");
	gameContext.pMaterialManager->AddMaterial(siegMat, UnitMaterial::SiegeMat);
	
	auto rangedMat = new SkinnedDiffuseMaterial();
	rangedMat->SetDiffuseTexture(L"./TheGame/Textures/RangedMinion.dds");
	gameContext.pMaterialManager->AddMaterial(rangedMat, UnitMaterial::ArcherMat);
	
}

void UnitManager::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);


	//check minions health
	for (UINT i = 0; i < m_EnemyMinions.size(); ++i)
	{
		if (m_EnemyMinions.at(i) && m_EnemyMinions.at(i)->GetCurrentHealth() < 0)
		{
			XMFLOAT3 pos = m_EnemyMinions.at(i)->GetTransform()->GetPosition();
			ParticleManager::GetParticleManager()->SpawnParticleEffect(XMFLOAT3(pos.x / 2.f, pos.y / 2.f, pos.z / 2.f), .5f, "bloodSplat");
			RemoveChild(m_EnemyMinions.at(i));
			m_EnemyMinions.erase(m_EnemyMinions.begin() + i);
			
		}
	}

	for (UINT i = 0; i < m_FriendlyMinions.size(); ++i)
	{
		if (m_FriendlyMinions.at(i) && m_FriendlyMinions.at(i)->GetCurrentHealth() < 0)
		{
			XMFLOAT3 pos = m_FriendlyMinions.at(i)->GetTransform()->GetPosition();
			ParticleManager::GetParticleManager()->SpawnParticleEffect(XMFLOAT3(pos.x / 2.f, pos.y / 2.f, pos.z / 2.f), .5f, "bloodSplat");
			RemoveChild(m_FriendlyMinions.at(i));
			m_FriendlyMinions.erase(m_FriendlyMinions.begin() + i);
			
		}
	}


	//check distance
	for (UINT i = 0; i < m_EnemyMinions.size(); ++i)
	{
		if (m_EnemyMinions.at(i) && m_EnemyMinions.at(i)->GetTransform()->GetPosition().z < -300)
		{
			static_cast<TheGameScene*>(GetScene())->DealDamageToPlayerBase(5);
			RemoveChild(m_EnemyMinions.at(i));
			m_EnemyMinions.erase(m_EnemyMinions.begin() + i);
			
		}
	}

	for (UINT i = 0; i < m_FriendlyMinions.size(); ++i)
	{
		if (m_FriendlyMinions.at(i) && m_FriendlyMinions.at(i)->GetTransform()->GetPosition().z > 850)
		{
			static_cast<TheGameScene*>(GetScene())->DealDamageToEnemyBase(5);
			RemoveChild(m_FriendlyMinions.at(i));
			m_FriendlyMinions.erase(m_FriendlyMinions.begin() + i);
		}
	}	

}

Unit* UnitManager::GetClosestenemy(Unit *unitStart)
{
	Unit *closestEnemy = nullptr;

	if (m_EnemyMinions.size() == 0)
		return nullptr;
	closestEnemy = m_EnemyMinions[0];


	XMVECTOR curPos = XMLoadFloat3(&unitStart->GetTransform()->GetPosition());
	XMVECTOR tarPos = XMLoadFloat3(&closestEnemy->GetTransform()->GetPosition());

	XMFLOAT3 shortestLength;
	XMStoreFloat3(&shortestLength, XMVector3Length(tarPos - curPos));

	for (UINT i = 0; i < m_EnemyMinions.size(); i++)
	{
		XMVECTOR enemyPos = XMLoadFloat3(&m_EnemyMinions[i]->GetTransform()->GetPosition());
		XMFLOAT3 otherLength; 

		XMStoreFloat3(&otherLength, XMVector3Length(enemyPos - curPos));

		if (shortestLength.x > otherLength.x)
		{
			closestEnemy = m_EnemyMinions[i];
			shortestLength = otherLength;
		}
	}

	return closestEnemy;
}

Unit* UnitManager::GetClosestPlayerUnit(Unit *unitStart){

	Unit *closestEnemy = nullptr;

	if (m_FriendlyMinions.size() == 0)
		return nullptr;
	closestEnemy = m_FriendlyMinions[0];

	XMVECTOR curPos = XMLoadFloat3(&unitStart->GetTransform()->GetPosition());
	XMVECTOR tarPos = XMLoadFloat3(&closestEnemy->GetTransform()->GetPosition());

	XMFLOAT3 shortestLength;
	XMStoreFloat3(&shortestLength, XMVector3Length(tarPos - curPos));

	for (UINT i = 0; i < m_FriendlyMinions.size(); i++)
	{
		XMVECTOR enemyPos = XMLoadFloat3(&m_FriendlyMinions[i]->GetTransform()->GetPosition());
		XMFLOAT3 otherLength;

		XMStoreFloat3(&otherLength, XMVector3Length(enemyPos - curPos));

		if (shortestLength.x > otherLength.x)
		{
			closestEnemy = m_FriendlyMinions[i];
			shortestLength = otherLength;
		}
	}

	return closestEnemy;
}

Unit* UnitManager::CreateUnit(UnitType type, bool playerUnit){


	switch (type)
	{
	case UnitManager::SoldierType:
		return new Soldier(XMFLOAT3(0, 0, 0), 20, 10, 15, playerUnit);
		break;
	case UnitManager::ArcherType:
		return new Archer(XMFLOAT3(0, 0, 0), 20, 10, 100, playerUnit);
		break;
	case UnitManager::HorseType:
		return new Horse(XMFLOAT3(0, 0, 0), 20, 10, 20, playerUnit);
		break;
	case UnitManager::SiegeType:
		return new Catapult(XMFLOAT3(0, 0, 0), 20, 10, 150, playerUnit);
		break;
	default:
		break;
	}

	return new Unit(XMFLOAT3(0, 0, 0), 10, 10, 10,false);
}
void UnitManager::AddEnemyUnit(UnitType type, XMFLOAT3 position){

	Unit *newUnit = CreateUnit(type,false);
	AddChild(newUnit);
	m_EnemyMinions.push_back(newUnit);
	
	newUnit->SetTargetBase(XMFLOAT3(0, 0, -1000));
	newUnit->GetTransform()->Translate(position);
};

void UnitManager::AddFriendlyUnit(UnitType type, XMFLOAT3 position){

	Unit *newUnit = CreateUnit(type,true);
	AddChild(newUnit);
	m_FriendlyMinions.push_back(newUnit);
	
	newUnit->SetTargetBase(XMFLOAT3(0, 0, 1000));
	newUnit->GetTransform()->Translate(position);

};

void UnitManager::Reset(){

	for (UINT i = 0; i < m_EnemyMinions.size(); ++i)
	{
		if (m_EnemyMinions.at(i))
		{
			RemoveChild(m_EnemyMinions.at(i));
		}
	}

	m_EnemyMinions.clear();

	for (UINT i = 0; i < m_FriendlyMinions.size(); ++i)
	{
		if (m_FriendlyMinions.at(i))
		{
			RemoveChild(m_FriendlyMinions.at(i));
		}
	}

	m_FriendlyMinions.clear();
}