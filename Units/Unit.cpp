//Precompiled Header [ALWAYS ON TOP IN CPP]

#include "Base\stdafx.h"

#include "Unit.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "Physx/PhysxProxy.h"
#include "..\..\Materials\SkinnedDiffuseMaterial.h"
#include "..\..\Materials\DiffuseMaterial.h"
#include "Graphics\ModelAnimator.h"
#include "UnitManager.h"
#include "../GameComponents/Arrow.h"
#include "../GameComponents/Rock.h"

Unit::Unit(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit) :
m_EnemyBase(enemyBase),
m_MoveSpeed(speed),
m_RotationSpeed(rotSpeed),
m_Range(range),
m_Radius(5.f),
m_Height(2.5f),
m_playerUnit(playerUnit)
{

}

Unit::~Unit(void)
{

}

void Unit::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);


}


void Unit::Update(const GameContext& gameContext)
{
	auto animator = m_pModel->GetAnimator();
	if (!animator->IsPlaying()){
		animator->SetAnimation(L"Run");
		animator->Play();
	}

	auto unitManager = UnitManager::GetInstance();

	
	if (m_playerUnit)
		m_CurrentTarget = unitManager->GetClosestenemy(this);
	else
		m_CurrentTarget = unitManager->GetClosestPlayerUnit(this);
	
	
	if (m_CurrentTarget)
	{
		m_TargetPos = m_CurrentTarget->GetTransform()->GetPosition();
	}
	else
		m_TargetPos = m_EnemyBase;


	XMVECTOR curPos = XMLoadFloat3(&GetTransform()->GetPosition());
	XMVECTOR tarPos = XMLoadFloat3(&m_TargetPos);

	XMFLOAT3 length;
	XMStoreFloat3(&length, XMVector3Length(curPos - tarPos));

	if (length.x < m_Range && animator->GetClipName() != L"Attack"){
		if (m_Range == 100.f)
			ShootArrow();
		if (m_Range == 150.f)
			ShootRock();
		animator->SetAnimation(L"Attack");
		Attack();
	}
	if (animator->GetClipName() == L"Attack")
		return;

	XMVECTOR direction = tarPos - curPos;
	
	//look at position
	XMFLOAT3 rotation;
	XMFLOAT3 dirFloat;
	XMStoreFloat3(&dirFloat,direction);
	float lookAtAngle = atan2f(dirFloat.x, dirFloat.z);
	XMVECTOR rotationVector = XMLoadFloat3(&GetTransform()->GetUp()) * lookAtAngle * 180 / g_XMPi;
	XMStoreFloat3(&rotation,rotationVector);
	
	//movement
	direction = XMVector3Normalize(direction);
	XMVECTOR displacmentVector = direction * m_MoveSpeed * gameContext.pGameTime->GetElapsed();
	XMFLOAT3 displacementFloat;
	XMStoreFloat3(&displacementFloat,displacmentVector);

	//gravity
	float gravity = 9.81f;
	if (this->GetComponent<ControllerComponent>()->GetCollisionFlags() != PxControllerCollisionFlag::eCOLLISION_DOWN)
	{
		displacementFloat.y -= gravity*gameContext.pGameTime->GetElapsed();
	}
	else{
		displacementFloat.y = 0;
	}

	this->GetComponent<ControllerComponent>()->GetTransform()->Rotate(rotation);
	this->GetComponent<ControllerComponent>()->Move(displacementFloat);

}

XMFLOAT3 Unit::ScanForEnemy()
{
	//scans a box arround the unit;

	return XMFLOAT3(0,0,0);
}

XMVECTOR Unit::GetPosition(){
	return XMLoadFloat3(&GetTransform()->GetPosition());
}

void Unit::Attack()
{
	if (m_CurrentTarget)
		m_CurrentTarget->ReceiveDamage(m_Damage);
}

void Unit::ShootArrow()
{
	if (GetChild<Arrow>())
		RemoveChild(GetChild<Arrow>());

	auto arrow = new Arrow();
	AddChild(arrow);
	
	XMFLOAT3 direction;
	if (m_playerUnit)
		direction = GetTransform()->GetForward();
	else
		direction = XMFLOAT3(-GetTransform()->GetForward().x, -GetTransform()->GetForward().y, -GetTransform()->GetForward().z);

	arrow->GetTransform()->Translate(XMFLOAT3(direction.x * 10, 2.f, direction.z * 10));


	PxVec3 force = PxVec3(direction.x, direction.y, direction.z) ;

	arrow->GetComponent<RigidBodyComponent>()->AddForce(force*100,PxForceMode::eIMPULSE);
}

void Unit::ShootRock()
{
	if (GetChild<Rock>())
		RemoveChild(GetChild<Rock>());

	auto rock = new Rock();
	AddChild(rock);
	XMFLOAT3 direction;

	if (m_playerUnit)
		direction = GetTransform()->GetForward();
	else
		direction = XMFLOAT3(-GetTransform()->GetForward().x, -GetTransform()->GetForward().y, -GetTransform()->GetForward().z);

	rock->GetTransform()->Translate(XMFLOAT3(direction.x * 10, 10.f, direction.z * 10));


	PxVec3 force = PxVec3(direction.x, direction.y, direction.z);

	rock->GetComponent<RigidBodyComponent>()->AddForce(force * 50, PxForceMode::eIMPULSE);
}