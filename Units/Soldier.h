#pragma once
#include "Unit.h"




class Soldier : public Unit
{
public:

	Soldier(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit);

	virtual ~Soldier();

	void Initialize(const GameContext& gameContext);


protected:


private:


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Soldier(const Soldier& t);
	Soldier& operator=(const Soldier& t);
};


