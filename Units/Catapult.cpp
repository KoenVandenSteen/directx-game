//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "Catapult.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "Physx/PhysxProxy.h"
#include "Diagnostics/Logger.h"
#include "../Buildings/BuildingFactory.h"
#include "..\..\Materials\SkinnedDiffuseMaterial.h"
#include "..\..\Materials\DiffuseMaterial.h"
#include "Graphics\ModelAnimator.h"
#include "UnitManager.h"
Catapult::Catapult(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit) :
Unit(enemyBase, speed, rotSpeed, range, playerUnit)
{
	m_Health = 200.f;
	m_Damage = 250.f;
}

Catapult::~Catapult(void)
{

}

void Catapult::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Create controller
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto mat = physX->createMaterial(0, 0, 1);
	auto controller = new ControllerComponent(mat, m_Radius, m_Height, L"CataPult", PxCapsuleClimbingMode::eEASY);

	AddComponent(controller);
	//hero mesh5


	m_pModel = new ModelComponent(L"./TheGame/Meshes/CataPult.ovm");
	m_pModel->SetMaterial(UnitManager::UnitMaterial::SiegeMat);
	auto obj = new GameObject();
	obj->AddComponent(m_pModel);

	AddChild(obj);
	obj->GetTransform()->Scale(0.05f, 0.05f, 0.05f);
	obj->GetTransform()->Translate(0.f, -5.f, 0.f);
	obj->GetTransform()->Rotate(0.f, 180.f, 0.f);
	
	m_TargetPos = m_EnemyBase;

	GetTransform()->Translate(0, -500, 0);
}








