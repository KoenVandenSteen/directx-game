#pragma once
#include "Scenegraph/GameObject.h"


class Unit;


class UnitManager : public GameObject
{
public:

	UnitManager();

	virtual ~UnitManager();

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);

	enum UnitType
	{
		SoldierType = 0,
		ArcherType,
		HorseType,
		SiegeType
	};

	enum UnitMaterial
	{
		SoldierMat = 51,
		ArcherMat,
		HorseMat,
		SiegeMat
	};


	Unit* GetClosestenemy(Unit *scanUnit);
	Unit* GetClosestPlayerUnit(Unit *unitStart);
	void AddEnemyUnit(UnitType type, XMFLOAT3 position);
	void AddFriendlyUnit(UnitType type, XMFLOAT3 position);
	void Reset();
	static UnitManager *GetInstance(){ return m_UnitManager ? m_UnitManager : m_UnitManager = new UnitManager(); };


protected:

private:

	vector<Unit*> m_FriendlyMinions;
	vector<Unit*> m_EnemyMinions;
	Unit* CreateUnit(UnitType type,bool PlayerUnit);
	static UnitManager* m_UnitManager;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	UnitManager(const UnitManager& t);
	UnitManager& operator=(const UnitManager& t);
};


