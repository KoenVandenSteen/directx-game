#pragma once
#include "Unit.h"




class Archer : public Unit
{
public:

	Archer(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit);

	virtual ~Archer();

	void Initialize(const GameContext& gameContext);


protected:


private:


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Archer(const Archer& t);
	Archer& operator=(const Archer& t);
};


