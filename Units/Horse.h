#pragma once
#include "Unit.h"




class Horse : public Unit
{
public:

	Horse(XMFLOAT3 enemyBase, float speed, float rotSpeed, float range, bool playerUnit);

	virtual ~Horse();

	void Initialize(const GameContext& gameContext);


protected:


private:


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Horse(const Horse& t);
	Horse& operator=(const Horse& t);
};


